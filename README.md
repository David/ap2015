Applied programming for NLP 2015, course slides
================================================================================

Requirements
--------------------------------------------------------------------------------

- Pandoc >=1.18
- Latex + Tikz
- mtheme (<https://github.com/matze/mtheme>) in `res/tex/mtheme`
- Reveal.js (<https://github.com/hakimel/reveal.js>) in `res/js/revealjs`

Installation
--------------------------------------------------------------------------------

Clone repository.

Install Pandoc locally (using Cabal) and unpack the resources:

    make install

Set permissions for Apache. Run this command whenever you add a new resource:

    make permissions

If you change the content of the `res/` resources directory, update the distribution package before you push:

    make dist

Other users then just have to run `make install` again to update their resources.

Commands
--------------------------------------------------------------------------------

Compile all websites (not presentations):

    make site

Compile the presentations for all lectures (using reveal.js for HTML and mtheme for Beamer):

    make slide
    make beamer

Compile for a specific lecture in subdirectory `lectures`, e.g. `01`:

    make slide lecture=01
    make beamer lecture=01

Shorthand for slides and beamers:

    make lecture=01

Compile a tutorial (e.g. using the shorthand above):

    make lecture=01/tutorial

You can also change the engine for the HTML slides:

    make lecture=01/tutorial html-slides-engine=slidy

... or change the Beamer theme to any theme your Latex installation provides:

    make beamer beamer-theme=Hannover beamer-colortheme=seahorse
