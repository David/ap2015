---
title: News Corpus Generation and Category Classification - Course Project for Applied Programming for NLP 2015
author: ap-ASCII (BA+MA)
date: 'June 23 - July 24, 2015 (23:59 CEST)'
---

Create a large news corpus by crawling and scraping news articles from different sites and train a category classifier.

### Preprocessing

1. The starting point is exercise 06. Use your and your class mates' XML files containing URLs to news articles.
1. Retrieve (crawl and scrape) raw article text from at least 3 news sites (same language, German or English; get the URL files from your class mates) and 3 different categories each.
    - You will need one scraper per news domain, i.e., one for New York times, another one for The Guardian, etc.
    - The final corpus should contain at least 500k tokens, with at least 30K tokens per category.
1. Clean up the data. Make sure the data does not contain any residual markup or garbage words.
1. Split the data into three sets (training/ validation/ test).

Additional output: corpus file(s)

## Training

1. Use a Naive Bayes implementation (e.g. Python sklearn) to train a category classifier for a given piece of text.
1. Think about at least 3 different types of feature sets and evaluate all of them separately and in combination of each other on the validation set.

## Evaluation

1. Evaluate the classifier that reached the best performance on the validation set on the test data.
