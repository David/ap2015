---
title: Interpolation vs Back-off - Course Project for Applied Programming for NLP 2015
author: ap-KORA (BA+BA+MA)
date: 'June 23 - July 24, 2015 (23:59 CEST)'
---

Implement and train a uni-, bi-, and trigram language model and compare interpolation vs. back-off.

# Preprocessing

1. The starting point is exercise 06. Use your XML file containing URLs to news articles.
1. Retrieve (crawl and scrape) raw article text from 1 category.
    - The final corpus should contain at least 100K tokens.
1. Clean up the data. Make sure the data does not contain any residual markup or garbage words.
1. Split the data into three sets (training/ validation/ test).


# Training

1. Train a uni-, bi-, and trigram language model on the training set.
1. Train two language models combining the three single ngram language models.
  - The first makes use of back-off.
  - The second interpolates the single language models.
  - Use Laplace smoothing wherever necessary.
1. Use the validation set to tune the hyperparameters of your models.

# Evaluation

1. Evaluate each language model (5 in total) by computing the perplexity on the test set.
