---
title: Dream Machine - Course Project for Applied Programming for NLP 2015
author: ap-VTK (BA+BA+MA)
date: 'June 23 - July 24, 2015 (23:59 CEST)'
---

Implement and train ngram language models and plug them into a system that uses them to generate new text.

# Preprocessing

1. Get the Wallstreet Journal corpus. Preprocess it as needed.

# Training

1. Train one ngram language model with add-one smoothing for each n=[1,2,3]. Use the validation set to tune the hyperparameters of your system.
1. Build a generative "dreaming" system that uses a language model to produce word sequences (as it is used e.g. in mobile text processing systems).
    - The prediction of each new word is conditioned on the previous context.
    - Produce one text file for each model with at least 100 words each.
    - Note that you need to draw the next word from the language model's distribution given the previous context. I.e., do not just generate the most probable one.

# Evaluation

1. Evaluate each language model by computing the perplexity on the test set.
1. Compare text generation of all three models.
