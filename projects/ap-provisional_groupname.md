---
title: Domain-specific Language Models - Course Project for Applied Programming for NLP 2015
author: ap-provisional_groupname (BA+BA+BA)
date: 'June 23 - July 24, 2015 (23:59 CEST)'
---

Implement and train several domain-specific language models and evaluate them on in- and out-of-domain data.

### Preprocessing

1. The starting point is exercise 06. Use your XML file containing URLs to news articles.
1. Retrieve (crawl and scrape) raw article text from at least 3 different categories.
    - The final corpus should contain at least 50K tokens per category.
1. Clean up the data. Make sure the data does not contain any residual markup or garbage words.
1. Split the data into three sets (training/ validation/ test) per category.

### Training

1. Train one unigram language model with add-one smoothing per category. Use the validation set to tune the hyperparameters of your model.
1. Train similar multi-domain language model on the combination of all training sets. Use the combination of all validation sets to tune the hyperparameters of this model.

### Evaluation

1. Evaluate each domain language model on in-domain test data.
1. Evaluate each domain language model on all other category (out-of-domain) test data.
1. Evaluate the multi-domain language model on the combination of all test sets.
1. Compare the global language model performance to the performance of the combination of the single language models.
