Notes SePaGo
============
- fast keine Tags

Projekt
-------
- Gut: Klassen, Dokumentation, lesbarer Code
- kein UTF-8 beim laden, aber Dateien sind UTF-8
- Start-Skript?
- Preprocessing Pipeline? Vokabular enthaelt z.B. "Kürzungen,"
- LangId features? Dev-Set wurde wie verwendet?
- LangId: woher Stoppwortlisten?

create_dataset.py
  def read(self, pathToFile):
    
    f = open(pathToFile)                    #open the file
--->    lines_temp = [(line.strip('\n'), self.language) for line in f]  #split the file into the list of sentences
    # Problem??? Grosses Korpus in Hauptspeicher

--->    lines = list(set(lines_temp))
    # Was tut das?

    return lines

- Klassifikationsergebnis: "It is crazy, and he deserves far better.    de"
    - Wie besser machen?
- Klassifikationsergebnis: "The Seppänen report makes an excellent proposal there.    de"
    - Warum falsch klassifiziert? (ä)


classify_v3.py
    def classify(self, F_train, F_test, all_train_labels):
      print('Start classification...')
--->      classi = MultinomialNB()
        # Warum diesen Klassifikator?
        # Aus welchem Paket?
      classi.fit(F_train, all_train_labels)
      
      output = classi.predict(F_test)
      
      return output

trainNgram.py
    fullLine = "<s> " + line.strip() + " </s>"   # Start and end symbols to calculate probability of the first and last elements of a sentence
    # Was, wenn n > 2? mehrere <s> Tags?