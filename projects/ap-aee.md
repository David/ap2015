---
title: Custom vs. Existing Language Models - Course Project for Applied Programming for NLP 2015
author: ap-aee (BA)
date: 'June 23 - July 24, 2015 (23:59 CEST)'
---

Implement and train a language model and compare it to an existing implementation.

### Preprocessing

1. The starting point is exercise 06. Use your XML file containing URLs to news articles.
1. Retrieve (crawl and scrape) raw article text from 1 category.
    - The final corpus should contain at least 100K tokens.
1. Clean up the data. Make sure the data does not contain any residual markup or garbage words.
1. Split the data into three sets (training/ validation/ test).

### Training

1. Implement and train a bigram language model with add-one smoothing. Use the validation set to tune any hyperparameters of your model.
1. Download the srilm language modeling toolkit and train a bigram language model with add-one smoothing on the training set. The manpages of srilm can be found in http://www.speech.sri.com/projects/srilm/manpages/.

### Evaluation

1. Evaluate your final language model on the test set.
1. Evaluate the srilm model on the test set.
1. Compare the performances of both language models.
