---
title: Language-specific Language Models - Course Project for Applied Programming for NLP 2015
author: ap_SePaGo (BA+BA+BA)
date: 'June 23 - July 24, 2015 (23:59 CEST)'
---

Implement and train several language-specific language models. Build a simple language classifier and evaluate both the models and the classifier.

### Preprocessing

1. Download the Europarl corpus (<http://www.statmt.org/europarl>) for at least 3 languages of your choice. At least one language must be German or English.
1. Split the data into three sets (training/ validation/ test) per language.

### Training

1. Train one uni-gram language model with add-one smoothing per language. Use the validation set to tune the hyperparameters of your models.
1. Build a language identification system that classifies the language of a given piece of text. Use an existing Naive Bayes implementation for that (e.g., Python sklearn). Think about good features for that classifier.

### Evaluation

1. Create a test set that contains sentences of all 3 languages.
1. Evaluate the accuracy of your language identification system.
1. Evaluate each language model by computing the perplexity on the test set sentences that the language identification system identifies for that specific language.
