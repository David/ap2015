---
title: Comparison of Smoothing Techniques - Course Project for Applied Programming for NLP 2015
author: ap-xanadu (MA+MA)
date: 'June 23 - July 24, 2015 (23:59 CEST)'
---

Implement and train a language model and compare different smoothing techniques.

# Preprocessing

1. Get the Wallstreet Journal corpus. Preprocess it as needed.

# Training

1. Train one bigram language model for each smoothing technique. Use the validation set to tune the hyperparameters of your models.
    - Implement at least add-one smoothing, absolute discounting, and Kneser-Ney.
1. Download the srilm language modeling toolkit and train a bigram language model for every smoothing technique on the training set. The manpages of srilm can be found in http://www.speech.sri.com/projects/srilm/manpages/.

# Evaluation

1. Evaluate each language model by computing the perplexity on the test set.
1. Compare the performances of your and the srilm language models.
