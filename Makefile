SHELL     = /bin/bash
MAKESHELL = /bin/bash
MAKE      = make

SILENT  ?=

# Options

CSS ?= flatly
# CSS ?= darkly
# CSS ?= slate
# CSS ?= simplex
# CSS ?= paper
# CSS ?= yeti
# See http://www.bootstrapcdn.com/#bootswatch_tab

html-slides-engine ?= revealjs
# html-slides-engine = slidy

beamer-theme ?= m
beamer-colortheme ?= metropolis

lecture ?= *

CSS_PATH = //maxcdn.bootstrapcdn.com/bootswatch/3.3.4/$(CSS)/bootstrap.min.css


# Globals

DIR_SITES  = .
DIR_LECTURES = lectures/$(lecture)

SOURCE_SITES  = $(wildcard $(DIR_SITES)/index.md)
SOURCE_SLIDES = $(wildcard $(DIR_LECTURES)/index.md)
# SOURCE_SLIDES = $(shell find $(DIR_LECTURES) -name index.md)

TARGET_SITES   = $(call replace-suffix,.site,$(SOURCE_SITES))
TARGET_SLIDES  = $(call replace-suffix,.$(html-slides-engine),$(SOURCE_SLIDES))
TARGET_BEAMERS = $(call replace-suffix,.beamer,$(SOURCE_SLIDES))


# Functions

remove-suffix = $(1:%.md=%)
replace-suffix = $(addsuffix $(1),$(call remove-suffix,$(2)))

# A literal space.
space :=
space +=

join-with = $(subst $(space),$1,$(strip $2))
split-at = $(subst $1, ,$(dir $@))
relativized-dirpath = $(call join-with,/,$(patsubst %,..,$(call split-at,/,$(dir $1))))


# Targets

.SECONDEXPANSION:
.PHONY: install permissions dist all site slides beamer

default  : slides beamer
all      : site slides beamer

site   : $$(TARGET_SITES)
slides : $$(TARGET_SLIDES)
beamer : $$(TARGET_BEAMERS)

install:
	if [ -z "$$(which pandoc pandoc-citeproc)" ]; then \
		cabal update \
		&& cabal install pandoc pandoc-citeproc \
		&& @echo 'Add this to your shell config: export PATH=~/.cabal/bin:$$PATH'; \
	fi;
	unzip -d ./ dist/res.zip;
	if [ ! -d "~/.fonts" ]; then \
		mkdir -p ~/.fonts; \
	fi;
	unzip -d ~/.fonts/ dist/Fira.zip;

permissions:
	chmod -R --silent 775 res/{css,img,js} pres
	find lectures index.md Makefile LICENSE README.md .gitignore -type f -regex '.*' | xargs chmod -R --silent 770
	find lectures -type d | xargs chmod --silent 775
	chmod --silent 774 index.html
	find lectures -regextype sed -regex ".*\.\(pdf\|html\)" | xargs chmod --silent 774
	chmod -R --silent 770 res/{tex,template} dist

dist:
	zip -r dist/res.zip res

# Default requests for HTML to HTML sites (not e.g. slides)
%.html : %.html-site

%.site : %.md
	pandoc \
		--smart \
		--standalone \
		--chapters \
		--toc \
		--toc-depth=2 \
		--latex-engine=xelatex \
		--variable lang=ngerman \
		--variable include-before='<div class="container">' \
		--variable include-after='</div>' \
		--css $(CSS_PATH) \
		--output $*.html \
		$<

%.slidy : %.md
	pandoc \
		--smart \
		--mathjax \
		--standalone \
		--incremental \
		--section-divs \
		--slide-level=2 \
		--to slidy \
		--output $*.html \
		$<

%.config.html : res/template/$$(@F).template
	cat $< \
		| perl -pe 's/\$$relative-res-dir\$$/$(subst /,\/,$(call relativized-dirpath,$@))/g' \
		> $@

%.revealjs : %.md $$(@D)/revealjs.config.html
	pandoc \
		--smart \
		--mathjax \
		--standalone \
		--incremental \
		--section-divs \
		--slide-level=2 \
		--to revealjs \
		--variable revealjs-url=$(call relativized-dirpath,$@)/res/js/reveal.js \
		--include-after-body $(word 2,$^) \
		--output $*.html \
		$<

%.beamer : %.md
	export TEXINPUTS=`pwd`/res/tex//: \
	&& cd $(@D) \
	&& pandoc \
		--smart \
		--slide-level=2 \
		--to beamer \
		--latex-engine=xelatex \
		--variable theme=$(beamer-theme) \
		--variable colortheme=$(beamer-colortheme) \
		--variable classoption=protectframetitle \
		--output ../$(notdir $(*D)).pdf \
		$(<F)

# --variable classoption=usetitleprogressbar \
# --variable package=booktabs \
# --variable package=minted \
# --variable mintedstyle=trac \
# --variable colortheme=seahorse \
