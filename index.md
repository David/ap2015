---
title: Applied Programming for NLP (SS2015)
author: |
    David Kaumanns & Sebastian Ebert

    <small>[Center for Information and Language Processing (CIS)](http://www.cis.uni-muenchen.de)

    University of Munich</small>
date: \today
---

![](http://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/V_wordcloud_skill.png/640px-V_wordcloud_skill.png)

News
--------------------------------------------------------------------------------

- 26/08/15: **[Grades](projects/grades.txt) are online**
- 23/07/15: **Added schedule for presentations**
- 02/07/15: **Please check if you are registered for the exam**
- 30/06/15: **Updated information for project presentation** - latex template, updated time information
- 25/06/15: **Updated information for project presentation**
- 08/05/15: **Our code repo is online (again)** - <https://gitlab.cip.ifi.lmu.de/kaumanns/ap-2015>
- 28/04/15: **New location for lecture** - The Tuesday lecture (10:00-12:00) is now in **Gobi** (computer room LU112).
- 23/04/15: **New office hours** - Tuesdays 14:00-15:00 (Kaumanns), Wednesdays 13:00-14:00 (Ebert)
- 16/04/15: **New location for tutorial** - The Thursday tutorial (16:00-18:00) is now in **Gobi** (computer room LU112).

Overview
--------------------------------------------------------------------------------

This course teaches practical software engineering for real-life industrial natural language processing tasks. The students are guided through the pipeline of language processing, from raw text corpora via preprocessing and analysis to a final problem-solving application.

**Learning goals**:

- Useful software engineering techniques for NLP problems
- Outlining and structuring typical NLP software projects
- Efficient implementation of various algorithms

**Requirements**: Proficiency in a modern programming language, such as *Perl*, *Python*, *Ruby*, *C++* or *Java*.

**Time & locations**:

- Lectures: Tuesdays 10:00-12:00, Gobi, 14/04/2015-14/07/2015
- Tutorials: Thursdays 16:00-18:00, Gobi, 16/04/2015-16/07/2015

**Course language**: German/English

**Programming language**: Instructional examples are given mainly in *Perl* and *Python*. For the assignments, students are welcome to choose any (common) languages in which they feel comfortable. However, we strongly recommend script languages, such as *Perl*, *Python*, *Ruby*, *Lua* or *Bash/Shell*. For compiled and/or more exotic languages, please refer to us first.

**Exercises**: Assignments are given on a weekly or biweekly basis and are implemented by groups of 2 or 3 students each.

**Contact**:

- Kaumanns: Office C 126, Tuesdays 14:00-15:00, kaumanns [æt] cis.lmu.de
- Ebert: Office C 107, Wednesdays 13:00-14:00, ebert [æt] cis.lmu.de

... or send us an email and ask when we are available.

### Grading Policy

To qualify for a graded certificate, each student is expected to

- complete at least 80% of the *assignments*
- give one *15min presentation*

The final grade is based on:

- Bachelor: *semester project* & *project defense*
- Master: *extended semester project* & *project defense*

The **semester project** will be a practical application of our course work, embedded within a typical NLP task. The exact assignment will be determined in the course of the semester.

The **project defense** will be a short presentation about the semester project. Each member of the group will outline her contribution to the project and answer questions by the instructors.

### Submission Policy

Completed assignments are to be submitted via [**CIP Gitlab**](https://gitlab.cip.ifi.lmu.de).
The last commit for each assignment must have a time stamp from before the due date.

Setup:

1. Before first use: activate your CIP Gitlab account on [CipConf](https://tools.rz.ifi.lmu.de).
1. Determine one group member to maintain the group repository.
1. Create a new project "ap-[GROUP NAME]".
1. Go to *Settings -> Members* and add your group members with *Developer* or *Master* privileges.
1. Give us (the instructors) access by
    - either making the project public at *Project -> Visibility Level*
    - or adding us (David Kaumanns, Sebastian Ebert) as new members with *Reporter* privileges.
1. Email us the link to the project repository and the group's names and email addresses.


### 15min presentations

Each student will give a short presentation of 15 minutes in the course of the semester. The topics will be extensions of the previous session. Students can propose their own topics up to two weeks before the presentation. Else they will be assigned by us.

The presentation slides are due at the lesson before the actual presentation, such that they can be reviewed by us and, if necessary, revised. The slides will be uploaded here after the presentation.

Formal requirements:

- One file per person (PDF)
- Title page with topic, name and date
- Second page with table of contents
- Penultima page with summary
- Last page with references


Syllabus
--------------------------------------------------------------------------------

1. Programming for NLP
1. Project management and meta data
1. Corpora retrieval and preparation
1. Web crawling and scraping
1. Linguistic preprocessing
1. Vocabulary generation and application
1. Software engineering toolbox for linguists
1. Deterministic algorithms
1. Stochastic algorithms (machine learning)
1. Evaluation
1. Probabilistic language models


Semester Project
--------------------------------------------------------------------------------

Due: Friday 24/07/2015, 23:59 CEST

Poster presentation: Thursday 30/07/2015

- poster: A0 landscape ([Latex template](projects/poster_template-latex.zip), main file is poster.tex)
- content: group name, authors, task description, data, your approach, your results
- presentation: 5 minutes per Bachelor student, 7 minutes per Master student (e.g., group of 1 B. and 1 M. needs to present 12 minutes)
- 10-15 minutes questions after your presentation


Schedule

- 30/07/2015 9:00 - 9:30: xanadu
- 30/07/2015 9:30 - 10:00: provisional
- 30/07/2015 10:00 - 10:30: aee
- 30/07/2015 13:00 - 13:30: ASCII
- 30/07/2015 13:30 - 14:00: KORA


---

Instructors' code repo: <https://gitlab.cip.ifi.lmu.de/kaumanns/ap-2015>

Lectures
--------------------------------------------------------------------------------

- Press ? for navigation help on the HTML slides
- Capital letters in parentheses are acronyms for presenters' names.

---

### Week 01

#### Lecture 14/04/15 - Programming for NLP (Kaumanns)

- Introduction & overview
- Pipelines & applications

Slides: [HTML](lectures/01) [PDF](lectures/01.pdf)

#### Tutorial 16/04/15 (Ebert)

- Open source projects
- Git & Gitlab
- NLP Resources

Slides: [HTML](lectures/01/tutorial) [PDF](lectures/01/tutorial.pdf) [git_handson.sh](lectures/01/tutorial/git_handson.sh)

#### Exercise 01: **Hello CIS** (due Thursday 23/04/15, 16:00)

1. Create a course project repository in CIP Gitlab (see instructions in the slides). Add your group members and us.
1. Create the skeleton directory structure.
1. Create a simple *Hello world* app in your designated programming language, along with an executable and a basic readme and/or Makefile to compile (if necessary) and run.
1. Stage, commit, push.
1. Tag the correct commit hash with name "ex_01"

---


### Week 02

#### Lecture 21/04/15 - Resource Pipeline (Kaumanns)

- NLP resources & formats
- How to design a pipeline
- Shell magic: sed, awk, grep, sort, tr, unique, ...
- Creating some clean resources from raw corpora

Slides: [HTML](lectures/02) [PDF](lectures/02.pdf)

#### Exercise 02: **resource pipeline** (due Thursday 30/04/15, 16:00)

Setup/finish you preprocessing pipeline. Design it in a modular way such that tasks that are independent are handled by separate calls (e.g. wget and subsequent preprocessing). Don't cram them all into one script. It should be very ease to choose another source and rerun your preprocessing pipeline.

So far, no third-party applications/scripts are allowed.

It is most important to setup a working pipeline than to create a perfect clean-up procedure. You may destroy sentences to a certain extent, as long as the final result is free of noise and garbage.

Pipeline:

1. Download these two sources:
    - <http://dumps.wikimedia.org/enwiki/20150304/enwiki-20150304-pages-articles1.xml-p000000010p000010000.bz2>
    - <http://dumps.wikimedia.org/enwiki/20150304/enwiki-20150304-pages-articles2.xml-p000010002p000024999.bz2>
1. Parse, extract and clean the text chunks (articles).
3. Create three resources:
    - Lexicon (as frequency list)
    - Clean corpus of ordered words. Feel free to organize it with additional markup to separate conceptual regions (e.g. articles in Wikipedia).
    - POS-tagged corpus (see below)
4. Download and use the CIS TreeTagger: <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger>
    - Note that you might have to reorganize your corpus to fit the input format of the TreeTagger.
1. Add, commit, push.


---

### Week 03

#### Lecture 28/04/15 - Linguistic Preprocessing (Kaumanns)

- Project reviews
- Lessons learned: preprocessing is hard
- Basic make

- How to preserve sentences
- (Semi-)Automatic annotation
    - Stanford Core NLP

Slides: [HTML](lectures/03) [PDF](lectures/03.pdf)

#### Presentations

- Unicode & UTF-8 [PDF](pres/unicode.pdf) (AW)
- Make [PDF](pres/make.pdf) (MH)

#### Tutorial 30/04/15 (Ebert)

- Wrapping up: shell wrapper & make

Slides: [HTML](lectures/03/tutorial) [PDF](lectures/03/tutorial.pdf)

#### Exercise 03: **Wrapping up your pipeline** (due Thursday 07/05/15, 16:00)

1. Download the Makefile template and use it to wire your scripts together.
    1. Refactor your scripts to be Makefile-friendly
        - One input file, one output channel
        - Both configurable via command line
        - E.g.: `clean.py input.txt > output.txt`
    2. Organize your scripts in a clean dependency chain.
        - Look for the right spot in the Makefile template and set in your scripts.
    3. Change the behaviour of the targets of `make corpus` and `make vocab`. Right now, these commands produce one file for each source. We want them to produce just two big files for corpus and vocab. Tipps:
        - Concatenate files via `cat` at the proper spots in the pipeline, as in `cat foo.txt bar.txt > foobar.txt`.
        - Think about sensible names for the two big files and use them as direct targets.
        - Install two new hooks to hide the file names from the user.
2. (Optional) Polish your preprocessing as much as possible. You need superclean data!
    - Are punctuations (sensibly) removed?
    - Are sentences kept intact?
    - No garbage/markup tokens left?
    - You may now these tools in addition to your own:
        - For parsing Wikipedia into clean XML: [WikiExtractor](https://github.com/bwbaugh/wikipedia-extractor)
        - For tokenization: [Stanford Tokenizer](http://nlp.stanford.edu/software/tokenizer.shtml), part of the [Stanford Parser Package](http://nlp.stanford.edu/software/lex-parser.shtml) ([Download for Java 7](http://nlp.stanford.edu/software/stanford-parser-full-2014-08-27.zip))
        - For POS tagging: [CIS TreeTagger](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger)
        - (Try installing them locally with `make install`. If it doesn't work, let me (David) know!)
3. Add, commit, push.

---

### Week 04

#### Lecture 05/05/15 - Interface, configuration, management (Ebert)

- Config files
- Command line vs. config files: when to use which?
    - Ex. lower-case switch

Slides: [HTML](lectures/04) [PDF](lectures/04.pdf)

#### Presentations

- Shell magic [PDF](pres/shell-magic.pdf) (IG)
    - tr, sed, grep, awk, sort, uniq, find, iconv, cut, man pages
- Binary & non-binary serialization [PDF](pres/serialization.pdf) [HTML](http://jbaiter.de/ap15-referat) (JB)
    - Python pickle, Matlab, XML, JSON, YAML, CSV, ...

#### Tutorial 07/05/15 (Kaumanns)

#### Exercise 04 - **Stanford Core NLP** (due Thursday 21/05/15, 16:00)

1. Download the Stanford Core NLP from <http://nlp.stanford.edu/software/corenlp.shtml> via make file.
1. Extend the architecture from last week's exercise in a way that the Stanford Core NLP is used on your cleaned Wikipedia data set.
    Use the tokenizer, the POS tagger, and the lemmatizer.
1. Using the shell, extract the tokens from the created file into a file (token file).
1. Using the shell, extract the lemmas from the created file into a file (lemma file).
5. Using the shell, count how often tokens and lemmas are equal and how often they are different (you can use 2 calls for that).
1. Write a programm that does the counting in the programming language of your choice.
    Use the two input files from above (token file and lemma file) and print the equal and difference counts to the command line.
1. Tag the correct commit hash with name "ex_04", push the tag

---

### Week 05

#### Lecture 12/05/15 - Tidy up

#### Presentations

- Advanced regexes [PDF](pres/regex_for_pros.pdf) (EN): <http://perldoc.perl.org/perlre.html>
    - Embedded code, replace, modifiers & flags
    - (negative) lookahead/ lookbehind, or: how to implement a cheap sentence splitter with one nifty regex
    - Precompilation and interpolation of regexes in Perl
- Logging: Python Logger [PDF](pres/logger.pdf) [HTML](pres/logger) (FP)

Slides: [HTML](lectures/05) [PDF](lectures/05.pdf)

#### Exercise 05 (optional) - **Command line parameters**

Use lowercase and min-freq options to control the behaviour of your pipeline. On possible solution:

Extend your scripts (e.g. clean and count) with two parameters:

- `--lowercase`: switch on lowercase (default: false)
- `--min-freq`: cut off words below a certain frequency (default: 0)

Wrap these options in make with variables:

```makefile
params ?=

%.corpus: %.xml
	src/clean.py $(params) $< > $@

%.vocab: %.corpus
	src/count.py $(params) $< > $@
```

```shell
make foo.corpus params="--lowercase"
make foo.vocab params="--min-freq 30"
```

Other solutions are welcome!

---

### Week 06

#### Lecture 19/05/15 - Crawling text

- Crawler
- APIs
- Scraping & parsing HTML

Slides: [HTML](lectures/06) [PDF](lectures/06.pdf)

- XML schemas
    - [Categories (cats.xsd)](lectures/06/cats.xsd)
        - [Example XML](lectures/06/cats-example.xml)
    - [URLs (urls.xsd)](lectures/06/urls.xsd)
        - [Example XML](lectures/06/urls-example.xml)
- [Validator script](lectures/06/validate_xml)
    - requires Python package `lxml` (install e.g. with `pip install lxml`)
    - call like `./validate_xml yourcats.xml cats.xsd`

#### Presentations

- Crawling & APIs: Twitter, Facebook [PDF](pres/APIs.pdf) (VD)
- Crawler basics [PDF](pres/web-crawling.pdf) (TT)
    - Netiquette: careful with Twitter & Google, Robots.txt
    - World Wild Web: Web standards: problems of crawling, improper HTML, ...
    - Libraries: Perl (Web scraper), Python (Beautifulsoup), ...
- HTML parsing [PDF](pres/html-parsing.pdf) (MS)
    - DOM vs. SAX vs. StAX

#### Exercise 06 - Scrape news categories (due Thursday 28/05/15, 16:00)

1. Use Web::Scraper/BeautifulSoup/Scrapy/wget to retrieve the main page of your designated news website.
1. Scrape the page for the main navigation element
1. Parse (or regex) the categories and sub-categories into our `cats.xsd` XML format.
    - Remember to set the `url` attribute for each (sub-)category.
1. Optional: Scrape each category site to retrieve a set of article links. Put them into our `urls.xsd` XML format. (We want to crawl them later.)
    - We need sensible values for the `id` attribute. Ideas?

News websites to choose from:

- German
    - <http://www.zeit.de> (assigned)
    - <http://www.spiegel.de/> (assigned)
    - <http://www.faz.net/> (assigned)
    - <http://www.bild.de/>
    - <http://www.taz.de/> (assigned)
        - Weird Javascript-driven sub-categoriest, but supposedly in same on same HTML.
- English
    - <http://www.theguardian.com> (assigned)
    - <http://www.nytimes.com> (assigned)
    - <http://www.sfgate.com>
    - <http://www.bbc.co.uk/news>
    - <http://www.news.com.au/>
        - Has sub-sub-categories, but do not need to be extracted.

---

### Week 07

#### Lecture 26/05/15 - Setting up experiments (Ebert)

- Data sets
    - Practical examples: How to split, what to look out for
- Training & test labels
    - Supervised vs unsupervised
    - Semi-automatic tagging
    - Homebrewed gold standard
- Cross-validation

Slides: [HTML](lectures/07) [PDF](lectures/07.pdf)

#### Presentations

- Supervised Word Sense Disambiguation [PDF](pres/word-sense-disambiguation.pdf) (YK, 28/05/15)

---

### Week 08

#### Lecture 02/06/15 - Decompounding I (Kaumanns)

- Introduction to decompounding for German

Slides: [HTML](lectures/08) [PDF](lectures/08.pdf)

#### Presentations

- Efficient regexes [PDF](pres/efficient-regexes.pdf) (PZ)
    - [Catastrophic Backtracking](http://www.regular-expressions.info/catastrophic.html)
    - [Efficient regexes](http://discovery.bmc.com/confluence/display/83/Writing+efficient+regular+expressions)
    - [atomic grouping](http://www.regular-expressions.info/atomic.html)
    - non-capturing groups
- Introduction to decompounding in German [PDF](pres/decompounding-for-german.pdf) (CC)

#### Tutorial (Kaumanns)

#### Exercise 08 - Article scraper

1. Scrape each category site from your last assignment to retrieve a set of article links.
  Put them into our urls.xsd XML format.
1. Take your urls.xml file and crawl all the included URLs.
1. Scrape the news text (headline, news text).
  Make sure you get the whole article, including articles that span multiple pages.
  Store the result into a file that's name is the id you chose for your links in the previous assignment.
1. Tag your commit in the repository.

Optional:

5. Preprocess the articles with your existing preprocessing pipeline (at least sentence splitting and tokenization).
1. Take the sentences of one category and split them into a training, development, and test set according to 80/10/10%.
  Remember to shuffle the data.
1. Do the same with the other categories.

Due: Thursday June 11, 2015, 16:00, i.e., the tag must point to a commit earlier than the deadline

#### Tutorial - Holiday

---

### Week 09

#### Lecture 09/06/15 - Decompounding II (Kaumanns)

Slides: [HTML](lectures/09) [PDF](lectures/09.pdf)

#### Presentations

- Unit tests [PDF](pres/unit-tests.pdf) (DW)

#### Tutorial (Kaumanns)

#### Exercise 09 - Decompounding (due Thursday 18/06/15, 16:00)

Schiller (2005) shows that for a large German news- paper corpus, 5.5% of 9,3 million tokens were identified as compounds. A decent decompounder is the foundation of many NLP applications for German.

Write a simple rule-based decompounder that takes a string (=compound candidate) as input and outputs a string of white-space-separated segments (head and modifier, no fugenelement). The algorithm doesn't have to be great. Just keep it easily extensible.

You may follow these guidelines (or do something totally different as long as it respects the interface):

### Preprocessing

1. Download the full set of gold-annotated compounds: <http://www.sfs.uni-tuebingen.de/lsd/compounds.shtml>
1. Download the German frequency list: <https://invokeit.wordpress.com/frequency-word-lists>
1. Derive a set of non-compounds from the frequency list using a handful of heuristics (e.g. min-length and max-length). The number of non-compounds should not be more than 2 times the number of compounds.
    - Cut the frequency list! Weird words and compounds are often lurking at the bottom of the frequency spectrum.
    - You might notice that you actually need a quick&dirty decompounder here to bootstrap a list of non-compounds. Recycle the code in your big decompounder. Later, you may use your (good) decompounder to create an even better list of non-compounds. Iterative improvement of the data set is allowed, as long as you repeat the following steps to create training set and test set.
1. Merge the compounds and non-compounds to get the data set. Shuffle it!
1. Split the data set into training set (80%) and test set (20%).

### Decompounder

1. Read the training set and test set (UTF-8) and store it in a (complex) data structure that maps each compound candidate to its segments. Remember that there is always at least one segment. There are several possible structures. Figure out which one works best for you.
1. For each compound candidate in the training set:
    1. Split the string into an array of characters. (Python provides a convenient `list` function.)
    1. Traverse the array end-to-start (equals right-to-left within string). You may also reverse the array first and then traverse start-to-end, as usual.
    1. For each character position:
        1. Apply your set of rules and decide whether it is a good position to split.
           The minimum recommended rule is: check the frequencies of the modifier candidate and the head candidate. If they are very high (=exceed a certain threshold), it looks good.
        1. Also, consider using hand-crafted lists, e.g.:
            - list of non-segments (e.g. for functional words, affixes and very short words)
            - list of non-compounds (e.g. proper nouns)
        1. If you split, check for common fugenelements to trim from the modifier. Again, it looks like a fugenelement, if the trimmed modifier has a high frequency.
    1. Print out the result, plus the gold annotation.
1. Review all splits/non-splits. Check for weaknesses and improve your set of rules. Be creative! Try some wild ideas.

At the end:

1. Run the decompounder over the test set. For each candidate, check if there was a split and if it was correct. Increment the respective true/false positive/negative evaluation counters.
1. Compute and print some simple evaluation statistics from your evaluation counters.


---

### Week 10

#### Lecture 16/06/15 - Classification (Ebert)

- Applications
- Naive Bayes

Slides: [HTML](lectures/10) [PDF](lectures/10.pdf)

#### Presentations

- Documentation: Python Sphinx (TTa)
    - (Javadoc, Doxygen, Perldoc, [Docutils](http://docutils.sourceforge.net))

#### Exercise 10 - Naive Bayes sentiment classifier
[sentiment corpus](./res/data/sstb_1000.tsv.bz2)

---

### Week 11

#### Lecture 23/06/15 - Unsupervised Learning (Ebert)

- Applications
- K-nearest neighbours
- kmeans

Slides: [HTML](lectures/11) [PDF](lectures/11.pdf)

#### Presentations

- Regexes: Unicode & posix classes & character classes (DB)
    - Why it is a bad idea to parse HTML with regexes
- Data structures & complexities (KS)

#### Exercise 11 - K-nearest neighbor of embeddings
[word embeddings](./res/data/word_embs.tsv.bz2)

---

### Week 12

#### Lecture 30/06/15 - Language models I (Ebert)

- ngram Language Models
- perplexity

Slides: [PDF](lectures/12.pdf)

#### Tutorial (Ebert)

- Q&A

#### Presentations

- Smoothing for n-gram language models (RH)

---

### Week 13

#### Lecture 07/07/15 - Language models II (guest)

- smoothing

Slides: [PDF](lectures/13.pdf)

#### Tutorial (Ebert)

- Q&A

#### Presentations

---

### Week 14

#### Lecture 14/07/15 - Language models III (Ebert)

- backoff
- interpolation
- misc

Slides: [PDF](lectures/14.pdf)

#### Tutorial (Ebert)

- Q&A

#### Presentations

- ! Performance optimization
    - Parallelization of preprocessing

