# Minimum specification

The following sections specify the minimum requirements for all projects.

## Setup

- The project code should be part of your existing Gitlab repo.
- Readme as Github Markdown
    - Installation/ dependencies
    - Instructions to run training and evaluation, including options and parameters.


## Interface

<training executable> [options...] [source...]

- Should use sensible default parameters (except for input files).
- At the beginning: prints out all parameters in human-readable form.

<evaluation executable> [options...] [source...]

- At the beginning: prints out all parameters in human-readable form.
- Prints out evaluation results in human-readable form.

Output: all generated model(s), evaluation as formatted plain text


- all language models: unigram + add-one smoothing


## Grading policy

- Specified hard and soft criteria
- Poster presentation


# Course projects

## News Corpus Generation and category classification (BA+MA, ap-ASCII)
@David: Only 2 people left???
@Seb: Yes, TP and YK.


Create a large news corpus by crawling and scraping news articles from different sites.

### Preprocessing

1. The starting point is exercise 06. Use your and your class mates' XML files containing URLs to news articles.
1. Retrieve (crawl and scrape) raw article text from at least 3 news sites (same language, German or English) and 3 different categories each.
    - You will need one scraper per news domain, i.e., one for New York times, another one for The Guardian, etc.
    - The final corpus should contain at least 500k tokens, with at least 30K tokens per category.
1. Clean up the data. Make sure the data does not contain any residual markup or garbage words.
1. Split the data into three sets (training/ validation/ test).

Additional output: corpus file(s)

## Training

1. Use a Naive Bayes implementation (e.g. Python sklearn) to train a category classifier for a given piece of text.

## Evaluation

1. Evaluate the classifier on the test data.



## Domain-specific Language Models (BA+BA+BA, ap-provisional_groupname)

Train several domain-specific language models and evaluate them on in- and out-of-domain data.

### Preprocessing

1. The starting point is exercise 06. Use your XML file containing URLs to news articles.
1. Retrieve (crawl and scrape) raw article text from at least 3 different categories.
    - The final corpus should contain at least 50K tokens per category.
1. Clean up the data. Make sure the data does not contain any residual markup or garbage words.
1. Split the data into three sets (training/ validation/ test) per category.

### Training

1. Train one domain language model per category. Use the validation set to tune the hyperparameters of your model.
1. Train a multi-domain language model on the combination of all training sets. Use the combination of all validation sets to tune the hyperparameters of your model.

### Evaluation

1. Evaluate each domain language model on in-domain test data.
1. Evaluate each domain language model on all other category test data.
1. Evaluate the multi-domain language model on the combination of all test sets.
1. Compare the global language model performance to the performance of the combination of the single language models.



## Language-specific Language Models (BA+BA+BA, ap_SePaGo)

Train several language-specific language models. Build a simple language classifier and evaluate both the models and the classifier.

### Preprocessing

1. Download the Europarl corpus (<http://www.statmt.org/europarl>) for at least 3 languages of your choice. At least one language must be German or English.
1. Split the data into three sets (training/ validation/ test) per language.

### Training

1. Train one language model per language. Use the validation set to tune the hyperparameters of your models.
1. Build a language identification system that classifies the language of a given piece of text.

### Evaluation

1. Create a test set that contains sentences of all 3 languages.
1. Evaluate the accuracy of your language identification system.
1. Evaluate each language model by computing the perplexity on the test set sentences that the language identification system identifies for that language.



## Cluster-specific Language Models (BA+MA+MA, ap-aee)

Cluster a news corpus into several domains and train domain-specific language models.

### Preprocessing

1. The starting point is exercise 06. Use your XML file containing URLs to news articles.
1. Retrieve (crawl and scrape) raw article text from at least 3 different categories.
    - The final corpus should contain at least 50K tokens per category.
1. Clean up the data. Make sure the data does not contain any residual markup or garbage words.
1. Cluster the articles of this news corpus using bag-of-words and k-means. Each cluster will represent one domain. Start with 3 clusters.
1. Split each cluster data into three sets (training/ validation/ test) per category.

### Training

1. Train one language model for each cluster. Use the validation set to tune the hyperparameters of your models.

### Evaluation

1. Evaluate the language model of each cluster.
1. qualitative evaluation of different language models (e.g., comparison of most frequent terms)
1. Train another language model on the combination of all cluster training sets. Use the combination of all cluster validation sets to tune the hyperparameters of your model.
1. Evaluate the new language model on the combination of all cluster test sets.
1. Compare the global language model performance to the performance of the combination of the single language models.



## Comparison of Smoothing Techniques (MA+MA, ap-xanadu)

Train a language model and compare different smoothing techniques.

### Preprocessing

1. Corpus (Wallstreet Journal???)
1. Split the data into three sets (training/ validation/ test) per category.

### Training

1. Train one language model for each smoothing technique. Use the validation set to tune the hyperparameters of your models.
    - At least add-one smoothing, Good-Turing, Kneser-Ney

### Evaluation

1. Evaluate each language model by computing the perplexity on the test set.



## Interpolation vs Back-off (BA+BA+BA, ap-KORA)

Train a uni-, bi-, and trigram language model and compare interpolation vs. back-off.

### Preprocessing

1. Corpus (Wallstreet Journal???)
1. Split the data into three sets (training/ validation/ test) per category.

### Training

1. Train all three language models.
1. Train two other language models, one being a back-off language model, the other one making use of interpolation.
1. Use the validation set to tune the hyperparameters of your models.

### Evaluation

1. Evaluate each language model by computing the perplexity on the test set.


## Dream Machine (BA+BA+MA, ap-VTK)

Train uni-, bi-, and tri-gram language models and plug them into a system that uses them to generate new text.

### Preprocessing

1. Corpus (Wallstreet Journal???)
1. Split the data into three sets (training/ validation/ test).

### Training

1. Train one language model for each n-gram (n=[1,2,3]). Use the validation set to tune the hyperparameters of your system.
    - Use add-one smoothing.
1. Build a generative "dreaming" system that uses a language model to produce word sequences (as it is used e.g. in mobile text processing systems).
    - The prediction of each new word is conditioned on the previous context.
    - Produce one text file for each model with at least 100 words each.
    - Note that you need to draw the next word from the language model's distribution given the previous context. I.e., do not just generate the most probable one.

### Evaluation

1. Evaluate each language model by computing the perplexity on the test set.
1. Compare text generation of all three models.



## Neural Network language model (optional)

## Comparison with baseline (SRI)
### Preprocessing
### Training
### Evaluation
