---
title: 'Week 14: Language Modeling III'
author: Sebastian Ebert
date: 14/07/2015
---

Today
================================================================================

---

- Language Modeling
    - backoff vs. interpolation
    - text generation
    - open vs. closed vocabulary
    - perplexity in log space
    - other types of LMs


Combining Models
================================================================================

---

- Is there a better way of estimating ngram probabilities for unseen ngrams?

. . .

- today: make use of lower order ngrams


Interpolation
--------------------------------------------------------------------------------

- = linear combination of different ngram orders

    $P_{li}(w_n | w_{n-2}, w_{n-1}) = \lambda_1 P_1(w_n) + \lambda_2 P_2(w_n | w_{n-1}) + \lambda_3 P_3(w_n | w_{n-2}, w_{n-1})$

. . .

- generally

    $P_{li}(w_n | w_1^{n - 1}) = \sum_{i=1}^n \lambda_i P_i(w_n | w_1^{n-i})$

    where $0 \leq \lambda_i \leq 1$ and $\sum_i \lambda_i = 1$

---

- generally applicable whenever probability functions must be combined
- result itself is probability function
- set weights by hand or automatically (e.g., by EM)


Backoff
--------------------------------------------------------------------------------

- instead of combining all ngram orders, use the longest one that is sufficiently reliable

\

    \footnotesize
    $P_{bo}\left(w_{n}|w_{n-i+1}^{n-1}\right)=\begin{cases}
    P_{smooth}\left(w_{n}|w_{n-i+1}^{n-1}\right) & \text{if\,}count\left(w_{n-i+1}^{n}\right)>k\\
    \alpha\left(w_{n-i+1}^{n-1}\right)P_{bo}\left(w_{n}|w_{n-i+2}^{n-1}\right) & \text{otherwise}
    \end{cases}$

---

- recursive process
- usually $k \in \{0, 1\}$
- $P_{smooth}$: smoothed probability; if this is Good-Turing we call $P_{bo}$ "Katz smooting"

. . .

- $\alpha$: normalizing factor, i.e., use only the probability mass that is left over

    $\alpha\left(w_{n-i+1}^{n-1}\right)=\frac{1-\sum_{w_{n}:count\left(w_{n-i+1}^{n}\right)>0}P_{smooth}\left(w_{n}|w_{n-i+1}^{n-1}\right)}{1-\sum_{w_{n}:count\left(w_{n-i+1}^{n}\right)>0}P_{smooth}\left(w_{n}|w_{n-i+2}^{n-1}\right)}$


Stupid Backoff
--------------------------------------------------------------------------------

\footnotesize
$S \left( w_{n} | w_{n-i+1}^{n-1} \right) = \begin{cases}
\frac{count(w_{n-i+1}^{n})}{count(w_{1-i+1}^{n-1})} & \text{if\,} count \left( w_{n-i+1}^{n} \right) > 0 \\
\alpha S \left(w_{n}|w_{n-i+2}^{n-1} \right) & \text{otherwise}
\end{cases}$

- $\alpha = 0.4$ for all $i$
- $S(w_n) = \frac{count(w_n)}{N}$, where $N$ is the sentence length


Interpolation vs. Backoff
--------------------------------------------------------------------------------

. . .

- similar for zero counts
    - lower-order distributions are used in determining the probability of n-grams
- different for non-zero counts
    - interpolation: use information from lower-order distributions
    - backoff: do not use information from lower-order distributions


Text Generation
================================================================================

Applications
--------------------------------------------------------------------------------

. . .

- generate image captions
- generate article headlines
- generate Chinese poetry
- spam generation


Process
--------------------------------------------------------------------------------

- start with a *seed* word, e.g., "\<s\>"
- filter all ngrams that begin with the previous $n-1$ words
- choose the next word

---

c(row|col)    i     want    to    read   a    book
----------- ------ ------  ----- ------ ---- ------
i             1     0       0      1     1    0
want         12     0       2      0     0    0
to            1     184     2      1     1    1
read          0     0       19     0     1    0
a             0     10      578    3     1    1
book          0     0       1      0     12   0

1. select the most probable one (the one with the highest count)
2. choose randomly
3. draw the word according to the proper multinomial distribution


Problems
--------------------------------------------------------------------------------

. . .

1. select the most probable one (the one with the highest count)
    - same text with same starting word
    - unigram model: always the same word, e.g., "the the the the"

2. choose randomly
    - same probability for all words, e.g., "embryogenesis" is as likely as "the"


Vocabulary
================================================================================

Why Unknown Words?
--------------------------------------------------------------------------------

. . .

- unknown word = a word we have not seen during training
- occurs because languages are productive or lack of training data


Closed Vocabulary
--------------------------------------------------------------------------------

- vocabulary is fixed
- ignore ngrams containing an unknown word
- i.e., treat them as if they weren't there

. . .

- problem
    - the smaller the predefined vocabulary, the less ngrams are evaluated
    - extreme case: vocabulary with only 1 word: lowest perplexity


Open Vocabulary
--------------------------------------------------------------------------------

- introduce special unknown token "\<UNK\>"
- map all unknown words to this token
- $\rightarrow$ estimate unknown token as all other tokens
- multiple unknown word types, e.g., *NUMBER*, *PROPER_NAME*

. . .

- problem
    - too small vocabulary
    - unknown word are more probable
    - $\rightarrow$ LM assigns higher probability
    - $\rightarrow$ lower perplexity


Recommendations
--------------------------------------------------------------------------------

- use an open vocabulary
- only compare language models with the same vocabulary!
- specify how you created the vocabulary


Perplexity in Log Space
================================================================================

---

- "underflow"
    - multiplication of many small probabilities leads to zero probabilities
    - solution: work in log space
    - $p_1 \times p_2 \times p_3 = \log p_1 + \log p_2 + \log p_3$
    - $P(S) = \prod_i P(w_i | w_1^{i-1})$
    - $\log P(S) = \sum_i \log P(w_i | w_1^{i-1})$

---

\begin{align*}
    PP(S) & = \sqrt[N]{\frac{1}{\prod_{i=1}^{N} P(w_i | w_1^{i-1})}} & \\
    & = \left( \prod_{i=1}^{N} P(w_i | w_1^{i-1}) \right)^{-\frac{1}{N}} & \left( x = 2^{\log_2 x} \right) \\
    & = 2^{ \log_{2} \left( \prod_{i=1}^{N} P(w_i | w_1^{i-1}) \right)^{-\frac{1}{N}}} & \left( \log x^y = y \log x\right) \\
    & = 2^{-\frac{1}{N} \log_{2} \prod_{i=1}^{N} P(w_i | w_1^{i-1})} & \\
    & = 2^{-\frac{1}{N} \sum_{i=1}^{N} \log_{2} P(w_i | w_1^{i-1})} & \\
    & = 2^{-\sum_{i=1}^{N} \frac{1}{N} \log_{2} P(w_i | w_1^{i-1})} & \\
    & = 2^{H(S,P)} & \\
\end{align*}

- $N$: sentence length
- $H$: entropy


Other Types of Language Models
================================================================================

---

- class-based LM (Brown et al. 1992)
    - assign each word to a class, e.g., "class of week days"
    - $P(w_i | w_1^{i-1}) = P(w_i | c_i) P(c_i | c_1^{i-1})$
    - first compute the probability of the current class given all previous classes
    - then compute the probability of the word within the class
    - fewer parameters since fewer classes than words
    - classes are e.g., Brown clusters

---

- factored LM (Bilmes et al. 2003)
    - represent each word by $K$ features (*factors*)
    - $w_i \equiv \{f_i^1, ... f_i^K\}$
    - features are e.g., morphological classes, stems, roots
    - $P(S) = P(w_i | w_1^{i-1})$ $\rightarrow$ $P(S) = P(f_i^{1:K} | w_{1:i-1}^{1:K})$

---

- neural network LM (Bengio et al. 2003)
    - use a neural network to estimate probability
    - implicit smoothing
    - every word is represented by a $k$-dimensional vector
    - similar words have similar vectors

![](../../res/img/nnlm.pdf)\ 


--------------------------------------------------------------------------------

Have fun!
