---
title: 'Week 03: Wrapping Up - Tutorial'
author: David Kaumanns
date: April 30, 2015
---

# Today

---

- Presentation assignments
- Preprocessing is hard
- Wrapping up with Make

# Presentation assignments

# Preprocessing is hard

## Example vocab

...


# Wrapping up with Make

## Pipeline & hooks

![](../../../res/img/make-pipeline.png)

##

![](../../../res/img/its-dangerous-to-go-alone-take-this.jpg)

[Makefile template](Makefile) \
[Shell wrapper](ap)

---

Have fun.
