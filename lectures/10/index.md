---
title: 'Week 10: Text Classification'
author: Sebastian Ebert
date: 15/06/2015
---

Today
================================================================================

---

- Organizational things
- Presentation: Documentation
- Text Classification
- Naive Bayes

Organizational Things
================================================================================

---

- groups we know of
    - aee (ES, EE, AW)
    - ASCII (TT, YK)
    - KORA (MH, CC, JH)
    - provisional_groupname (PZ, DW, EN)
    - SePaGo (DP, IG, MS)
    - VTK (KS, TT, VD)
    - xanadu (JB, FP)
    - missing someone?


Presentation
================================================================================

Documentation


Text Classification
================================================================================

What is Classification?
--------------------------------------------------------------------------------

. . .

- assign a class (category) to an element (document, sentence, token, etc.)
- classes usually predefined
- thus, supervised learning (i.e., labels exist)


General Examples
--------------------------------------------------------------------------------

. . .

- distinguishing between cat and dog
- doing weather forecast
- finding a red ball among blue balls
- distinguishing between spam and ham emails
- recognize a character
- find grandma in a picture


NLP Examples
--------------------------------------------------------------------------------

. . .

- spam vs. ham
- part-of-speech tagging
- morphological tagging
- word sense disambiguation
- named entity recognition
- decompounding?
- polarity classification
- topic classification


Text Classification Pipeline
--------------------------------------------------------------------------------

![](../../res/img/supervised_ml.png)\ 


Examples
--------------------------------------------------------------------------------

. . .

- decompounding
- spam vs. ham
- part-of-speech tagging
- morphological tagging
- word sense disambiguation
- named entity recognition
- polarity classification
- topic classification


Classification Methods
--------------------------------------------------------------------------------

. . .

- rule-based
    - word ends with "ed" $\rightarrow$ "VBD"
    - list of positive and negative words
    - list of spam addresses
    - high accuracy
    - high cost
- machine learning methods
    - let the computer do it for you
    - requires labeled examples

Machine Learning Classification Methods
--------------------------------------------------------------------------------

- Which do you know?
- Which have you worked with?

---

- fixed set of classes: $C = \{c_1, c_2, \ldots, c_m\}$
- fixed set of elements (e.g., documents): $D = \{d_1, \ldots, d_n\}$
- training examples: $\langle d_j, c_i \rangle$
- learn function that predicts the class of a new example: $\phi : D \rightarrow C$
- $\phi$ is called *model* or *classifier*


Naive Bayes
================================================================================

---

- based on the Bayes's rule

. . .

\begin{align*}
P(c|d) & = \frac{P(d|c)P(c)}{P(d)}
\end{align*}

\begin{align*}
posterior & = \frac{likelihood * prior}{evidence}
\end{align*}

. . .

\begin{align*}
\ensuremath{c} & = argmax_{c}\frac{P(d|c)P(c)}{P(d)}\\
 & = argmax_{c} P(d|c)P(c)
\end{align*}

---

- document $d$ is represented by a vector of features: \
  $d \in \mathbb{N}^k$ $\rightarrow$ $d = [x_1 x_2 \ldots x_k]$
- independence assumption: \
  $x_i$ and $x_j$ are independent given class $c$

\begin{align*}
P(d|c)P(c) & =P(x_{1},x_{2},...,x_{k}|c)P(c)\\
 & =P(x_{1}|c)P(x_{2}|c)\ldots P(x_{k}|c)P(c)\\
 & =P(c)\prod_{i=1}^{k}P(x_{i}|c)
\end{align*}

---

\begin{align*}
P(d|c)P(c) & =P(c)\prod_{i=1}^{k}P(x_{i}|c)
\end{align*}

- learn parameters by maximum likelihood, i.e., simply count frequencies
- how many elements do belong to class $c$
\begin{align*}
P(c) = \frac{count(C = c)}{n}
\end{align*}

- in class $c$: what is the fraction of $x_i$ compared to all features in this class
\begin{align*}
P(x_{i}|c) = \frac{count(x_{i},c)}{\sum_x count(x, c)}
\end{align*}


Example
--------------------------------------------------------------------------------

\begin{tabular}{llll}
& Doc & Words & Class \\
Training & 1 & Munich Bremen & g \\
& 2 & Munich Munich & g \\
& 3 & Munich Bremen & g \\
& 4 & France Lyon Lyon Munich & f \\
\end{tabular}

\begin{align*}
P(c) = \frac{count(C = c)}{n}
\end{align*}

. . .

\begin{align*}
P(g) & = 3/4\\
P(f) & = 1/4
\end{align*}

---

\begin{tabular}{llll}
& Doc & Words & Class \\
Training & 1 & Munich Bremen & g \\
& 2 & Munich Munich & g \\
& 3 & Munich Bremen & g \\
& 4 & France Lyon Lyon Munich & f \\
\end{tabular}

\begin{align*}
P(x_{i}|c) = \frac{count(x_{i},c)}{\sum_x count(x, c)}
\end{align*}

. . .

\begin{align*}
P(Munich|g) = 4/6\\
P(Munich|f) = 1/4\\
P(Lyon|f) = 2/4\\
P(Lyon|g) = 0/6
\end{align*}

---

\begin{tabular}{llll}
& Doc & Words & Class \\
Training & 1 & Munich Bremen & g \\
& 2 & Munich Munich & g \\
& 3 & Munich Bremen & g \\
& 4 & France Lyon Lyon Munich & f \\
Test & 5 & Munich Munich & ? \\
\end{tabular}

\begin{align*}
P(c | d) & = P(d|c)P(c)
\end{align*}

. . .

\begin{align*}
P(g | d_5) & = 3/4 * 4/6 * 4/6 = 48/144\\
P(f | d_5) & = 1/4 * 1/4 * 1/4 = 1/64\\
\end{align*}

. . .

$\rightarrow argmax_{c}$ is *g*


---

\begin{tabular}{llll}
& Doc & Words & Class \\
Training & 1 & Munich Bremen & g \\
& 2 & Munich Munich & g \\
& 3 & Munich Bremen & g \\
& 4 & France Lyon Lyon Munich & f \\
Test & 5 & Munich Munich & ? \\
 & 6 & Munich Munich Munich Lyon & ? \\
\end{tabular}

\begin{align*}
P(c | d) & = P(d|c)P(c)
\end{align*}

. . .

\begin{align*}
P(g | d_6) & = 3/4 * 4/6 * 4/6 * 4/6 * 0/6 = 0/5184 \\
P(f | d_6) & = 1/4 * 1/4 * 1/4 * 1/4 * 2/4 = 2/1024\\
\end{align*}

. . .

$\rightarrow argmax_{c}$ is *f*

---

- zero conditional probability problem
- instead of 
\begin{align*}
P(x_{i}|c) = \frac{count(x_{i},c)}{\sum_x count(x, c)}
\end{align*}

- use add-one smoothing
\begin{align*}
\hat{P}(x_{i}|c) & = \frac{count(x_{i},c) + 1}{\sum_x count(x, c) + |V|}
 & = \frac{count(x_{i},c) + 1}{\sum_x count(x, c) + k}
\end{align*}

---

\begin{tabular}{llll}
& Doc & Words & Class \\
Training & 1 & Munich Bremen & g \\
& 2 & Munich Munich & g \\
& 3 & Munich Bremen & g \\
& 4 & France Lyon Lyon Munich & f \\
\end{tabular}

\begin{align*}
\hat{P}(x_{i}|c) = \frac{count(x_{i},c) + 1}{\sum_x count(x, c) + k}
\end{align*}

. . .

\begin{align*}
P(Munich|g) = (4 + 1)/(6 + 4) = 5/10 \\
P(Munich|f) = (1 + 1)/(4 + 4) = 2/8 \\
P(Lyon|f) = (2 + 1)/(4 + 4) = 3/8 \\
P(Lyon|g) = (0 + 1)/(6 + 4) = 1/10 
\end{align*}


Assignment
================================================================================

Exercise 10 - Polarity Classification
--------------------------------------------------------------------------------

1. Download the sentiment corpus from the course website ([here](../../res/data/sstb_1000.tsv.bz2)).
  It's already preprocessed, so don't bother.
1. Cut off the first 2 columns, e.g., by using *cut* in the shell.
  The remaining text is the label and the text.
1. Split the data into training/dev/test sets.
1. Extract features that might help you to classify the polarity of an element.
  Features must be stored in a feature vector (the same length for all elements).
  Create at least 3 feature types, be creative.

more on the next slide!

---

5. Use a Naive Bayes implementation in a library of your choice (Python: NLTK, sklearn) to train a model.
1. While implementing new features, evaluate your model on the development set.
1. Choose the best feature set and evaluate the model on the test set.
  Log the performance of your model in a log file.
1. Tag your commit in the repository.

Due: Thursday June 25, 2015, 16:00, i.e., the tag must point to a commit earlier than the deadline

--------------------------------------------------------------------------------

Have fun!
