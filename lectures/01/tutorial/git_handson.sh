#!/bin/bash

# Setup the git project
git clone git@gitlab.cip.ifi.lmu.de:<user name>/<project name>.git
git config --global user.name "<your name here>"
git config --global user.email "<your email here>"

# Check the directory
cd ap-the_instructors
ll

# Create a reamde file
emacs readme.md
git status
git add readme.md
git status
git commit
git status
git push -u origin master
git status
emacs readme.md
git diff
git status
git commit -am "added date to readme"
git status

# Add exercise 1 solution
mkdir src
mkdir src/01
git status
cd src/01
emacs hello_word.py
python hello_word.py
git status
cd ..
cd ..
git status
git add src
git status
git commit -m "added hello world"
git push

# Check repository information
git log
git config --global alias.lga "log --pretty=format:'%C(auto)%h %C(110)%ad%Creset%C(auto)%d %s' --graph --date=short --all"
git lga
git tag "ex_01"
git push --tags
git lga

# Show collaboration capabilities of git
echo "I changed the readme in Gitlab to show collaboration with git"
git lga
git pull
git lga
# The following hash does not work for you. Use any hash of your project instead (see git lga).
git checkout 504f960
git lga
git checkout ex_01
git lga
git checkout master
git lga

# Add shell script that executes the Python script
git branch "ex_01_run"
git lga
git checkout ex_01_run
git lga
git status
mkdir bin
cd bin
emacs ex_01
chmod 770 ex_01
git lga
git status
git add ./
git commit -m "added start script for ex 01"
git lga
git push -u origin ex_01_run
cd ..

# Change something in the master branch to show merging.
git checkout master
emacs readme.md
git commit -am "updated readme"
git lga
git push
git status
git merge ex_01_run
git lga
git branch -d ex_01_run
git lga
git push
