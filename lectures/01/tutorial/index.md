---
title: 'Week 1 - Tutorial: Open Source & Git'
author: Sebastian Ebert
date: April 16, 2015
---


Today
================================================================================

---

- Organizational things
- Open source project structure
- Gitlab & Git
- Resources


Organizational Things
================================================================================

---

- new room for tutorials (TBD)
- warning: programming skills required
- Master students: email your name, matriculation number, and email address to us
- got your Gitlab password?
    - check at <https://webmail2.cip.ifi.lmu.de>
    - if necessary, set up email forwarding in [CipConf](https://tools.rz.ifi.lmu.de)
- students writing Bachelor's thesis: due date for grade?


Open Source Project Structure
================================================================================

---

- `src/`: source code
    - 01
    - 02
    - project
- `res/`: static (external) resource files
- `var/`: ever-changing files, e.g. logs
- `etc/`: configuration files

---

- `lib/`: external libraries
    - `perl/`
    - `python/`
    - `java/`
    - ...
- `build/`: compiled binaries
- `bin/`: executables, e.g. shell script wrapper
- `test/`: code for (unit) tests

---

- `doc/`: documentation
- `README.md`: Markdown-formatted readme
- `Makefile`: routines for compiling and/or installing (even script projects!)
- `LICENSE`: <http://choosealicense.com>
- `.gitignore`: <https://www.gitignore.io>


Git & Gitlab
================================================================================


Why Version Control?
--------------------------------------------------------------------------------

- collaboration of multiple project members from different places
- keep track of changes in project
- roll back changes easily
- keep different source versions with branching


Why Git?
--------------------------------------------------------------------------------

Benefits

- decentralized/distributed revision control
    - developers do not need to share a common network
    - work off-line until you want to publish your code
- widely used
- fast and easy branching and merging
- free and open source

---

Drawbacks

- steep learning curve
- (weird command names)
    - maybe at first
    - but then you learn where they come from


Why Gitlab?
--------------------------------------------------------------------------------

- Easy collaboration: branches, bug reports (issues), bug assignments
- Easy inspection for instructors
- Documentation support (markdown, Wiki)
- Everyone already has an account.


Task 1: Groups
--------------------------------------------------------------------------------

- form a group
- 2-3 people
- mix skill levels


Task 2: Setup Gitlab
--------------------------------------------------------------------------------

1. Before first use: activate your CIP Gitlab account on [CipConf](https://tools.rz.ifi.lmu.de).
1. CIP Gitlab: <https://gitlab.cip.ifi.lmu.de>
1. Create your SSH key (necessary for all your PCs)
1. Create a new project "ap-\[GROUP NAME\]".

---

5. Go to Settings -> Members and add your group members with Developer or Master privileges.
1. Give us (the instructors) access by
    - either making the project public at Project -> Visibility Level
    - or adding us (David Kaumanns, Sebastian Ebert) as new members with Reporter privileges.
1. Email us the link to the project repository, the group name and your email addresses.


Git Walk Through
================================================================================

Task 3: Exercise
--------------------------------------------------------------------------------

1. Create the skeleton directory structure.
1. Create a simple *Hello world* app in your designated programming language, along with an executable and a basic readme and/or Makefile to compile (if necessary) and run.
1. Stage, commit, push.
1. Tag the correct commit hash with name "ex_01"


Idea of Git
--------------------------------------------------------------------------------

![http://www.git-scm.com/book/en/v2/Getting-Started-Git-Basics](../../../res/img/git-areas.png)


Let's do it
--------------------------------------------------------------------------------

- see shell script [git_handson.sh](./git_handson.sh) for all commands and walk through


Commands you Need
--------------------------------------------------------------------------------

- Clone your project:
    - `git clone` `git@gitlab.cip.ifi.lmu.de:<user name>/<project name>.git`

---

- Do your changes.
- **Stage** your changes (i.e. tell Git that they exist):
    - `git add <file name|patterns>`

---

- **Commit** your changes to your **local** repository:
    - `git commit -am "initial commit"`
        - `-a (--all)`: automatically stage files that have been modified and deleted (only those Git already knows).
        - `-m (--message)`: use an inline commit message.
- **Push** your changes to the **remote** repository:
    - `git push`
        - For first push: `git push -u origin master`
- Do more changes. Repeat: stage, commit, push.

---

- Do fresh pulls regularly:
    - `git pull`
- Check your status:
    - `git status`
- Check differences
    - `git diff`
        - if problems with color occur: `git config --global core.pager "less -r"`

---

- Review previous commit
    - `git show`
- Use an alias for nicely formatted logs:
    - `git config --global alias.lga "log `
    `--pretty=format:'%C(auto)%h `
    `%C(110)%ad%Creset%C(auto)%d `
    `%s' --graph --date=short --all"`
    - `git lga`


Branching
--------------------------------------------------------------------------------

![Wittfind Web branches](../../../res/img/wittfind-web-branches.png)

---

- Create new branch:
    - `git branch awesome-feature`
- Switch to new branch:
    - `git checkout awesome-feature`
        - (Shorthand for last two steps: `git checkout -b awesome-feature`)

---

Ready to merge your new feature into the master branch?

- Switch to the master branch:
    - `git checkout master`
- Merge your branch into the current one (master):
    - `git merge awesome-feature`
- Delete the deprecated branch:
    - `git branch -d awesome-feature`

---

Words to remember

- *HEAD*: pointer to your current position in the history
- *ORIGIN*: original remote repository
- *master*: the (hopefully) stable master branch
- *upstream*: usually means the remote repository, i.e., where the code is coming from ("up the stream") or where you push to and pull from
- *fast-forward*: moving the HEAD pointer forward in history
- *.gitignore*: list of stuff to ignore

Great Git tutorials
--------------------------------------------------------------------------------

- <http://www.git-scm.com/doc>
- <http://gitimmersion.com>


Resources
================================================================================

---

![Which resources do you know?](../../../res/img/nlp-pipeline.png)


Lexicons
--------------------------------------------------------------------------------

- Multi-language frequency lists
    - <https://invokeit.wordpress.com/frequency-word-lists>
    - created from subtitles
- English frequency list
    - <http://www.wordfrequency.info/free.asp>
    - created from Corpus of Contemporary American English
- The CMU Pronouncing Dictionary
    - <http://www.speech.cs.cmu.edu/cgi-bin/cmudict>
- MPQA Subjectivity Clues
    - <http://mpqa.cs.pitt.edu/lexicons/subj_lexicon/>
- CISLex


Treebanks
--------------------------------------------------------------------------------

    (S
        (NP
            (NNP John)
        )
        (VP
            (VPZ loves)
            (NP
                (NNP Mary)
            )
        )
        (...)
    )

---

- The Penn Treebank Project
    - <https://www.cis.upenn.edu/~treebank/>
- German Treebank: Tiger
    - <http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/tiger.en.html>
- SMULTRON - Stockholm MULtilingual TReebank
    - <http://www.cl.uzh.ch/research/parallelcorpora/paralleltreebanks/smultron_en.html>
- University of Arizona TreeBank Viewer
    - <http://www.dingo.sbs.arizona.edu/~sandiway/treebankviewer/index.html>

Knowledge bases & ontologies
--------------------------------------------------------------------------------

- WordNet
    - <https://wordnet.princeton.edu>
- Germanet
    - <http://www.sfs.uni-tuebingen.de/GermaNet>
- Chinese Wordnet, BalkaNet, IndoWordNet, FinnWordNet, ...
- DBpedia
    - machine readable Wikipedia content
    - knowledge base of > 4 million "things"
    - <http://wiki.dbpedia.org/Ontology>

Parallel text corpora
--------------------------------------------------------------------------------

- Europarl
    - proceedings of the European Parliament in 21 languages
    - <http://www.statmt.org/europarl/>
- WMT11
    - from a shared task
    - <http://www.statmt.org/wmt11/translation-task.html#download>

Questions & answers
--------------------------------------------------------------------------------

- Microsoft Research Question-Answering Corpus
    - text of Encarta 98
    - <http://research.microsoft.com/en-us/downloads/88c0021c-328a-4148-a158-a42d7331c6cf/>
- PASCAL RTE datasets for textual entailment tasks
    - <http://pascallin.ecs.soton.ac.uk/Challenges/RTE/Datasets/>

Collocations & NGrams
--------------------------------------------------------------------------------

- Google Books Ngram Corpus
    - <http://storage.googleapis.com/books/ngrams/books/datasetsv2.html>
- <http://corpus.byu.edu>
- English collocations
    - <http://collocations.ooz.ie/>
- CIS Wittfind
    - <http://wittfind.cis.uni-muenchen.de>

Pretrained models & representations
--------------------------------------------------------------------------------

- Stanford Core NLP
    - tokenizer, sentence splitter, POS tagger, coreference resolution, etc.
    - <http://nlp.stanford.edu/software/corenlp.shtml>
- Polyglot word embeddings
    - low dimensional word representations for many Wikipedia languages
    - <https://sites.google.com/site/rmyeid/projects/polyglot#TOC-Download-the-Embeddings>

Text corpora
--------------------------------------------------------------------------------

- UMBC WebBase corpus
    - <http://ebiquity.umbc.edu/resource/html/id/351>
- News articles
    - Reuters news: <http://trec.nist.gov/data/reuters/reuters.html>
    - Wall Street Journal: <https://catalog.ldc.upenn.edu/LDC94S13A>
    - North American News Text Corpus: <https://catalog.ldc.upenn.edu/LDC95T21>
- Wikipedia

Wikipedia
--------------------------------------------------------------------------------

- Raw dumps: <https://dumps.wikimedia.org>
- preprocessing scripts: Wikipedia Extractor
    - <http://medialab.di.unipi.it/wiki/Wikipedia_Extractor>
- Preprocessed Wikipedia dumps
    - <https://sites.google.com/site/rmyeid/projects/polyglot#TOC-Download-Wikipedia-Text-Dumps>

Assignment
================================================================================

Exercise 01 - Hello CIS
--------------------------------------------------------------------------------

1. Create a course project repository in CIP Gitlab (see instructions above). Add your group members and us.
1. Create the skeleton directory structure.
1. Create a simple *Hello world* app in your designated programming language, along with an executable and a basic readme and/or Makefile to compile (if necessary) and run.
1. Stage, commit, push.
1. Tag the correct commit hash with name "ex_01"

Due: Thursday April 23, 2015, 16:00, i.e., the tag must point to a commit earlier than the deadline

--------------------------------------------------------------------------------

Have fun
