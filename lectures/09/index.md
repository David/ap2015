---
title: 'Week 09: Decompounding II'
author: David Kaumanns
date: 09/06/2015
---

Today
================================================================================

---

- Presentation: Unit tests
- Reductionism and rule-based systems
- Framework for decompounding
- Evaluation

About reductionism in NLP
================================================================================

---

![](../../res/img/duck-machine.jpg)

---

- NLP problems have to be reduced in order to be solved
- There are two main types of reductionist approaches in NLP:

1. Statistical machine learning
    - Feed raw data into a complex decision system.
    - Let the computer do the reduction.
1. Rule-based systems
    - Heavily preprocess the data and/or devise complex rules to solve the problem.
    - Simple decision system (often only based on a threshold).
    - We do the the reduction.

---

![](../../res/img/reductionism-xkcd.png)

Framework for decompounding
================================================================================

---

![](../../res/img/nlp-pipeline.png)

---

![](../../res/img/decompounder-framework.png)

## Preprocessing pipeline:

1. Collect non-compounds from frequency list.
    - 6 characters to 15 characters per word
2. Merge gold splits and non-compounds.
    - ratio 1/3 to 2/3
    - into same file, you may want to correct false splits later
3. Shuffle the data.
4. Split the data into training set and test set.
    - ratio 80% to 20%

---

5. Run your system over the data:
    - Training data: print out splits as human-readable output
    - Test data: print out evaluation results (see some slides later)
6. Review the training data splits.
7. Correct and/or extend your splitting rules.
8. Goto 5.

## How to handle fugenelements

![](../../res/img/fugenelements.png)

---

- Start resolving the most frequent fugenelements.
- Skip fugenelements that are too difficult for now.

Evaluation
================================================================================

## Measurement

. . .

Precision

. . .

tp / ( tp + fp )

. . .

Recall

. . .

tp / ( tp + fn )

. . .

See also <http://en.wikipedia.org/wiki/Precision_and_recall>

## True/false positives/negatives in decompounding

- true positives
    - compounds that were split correctly
- true negatives
    - non-compounds that were not split
- false positives
    - non-compounds that were split + compounds that were split incorrectly
- false negatives
    - compounds that were not split + compounds that were split incorrectly

(Alfonseca et al., 2008)

Discussion
================================================================================

---

- Characterwise analysis
    - reverse!

- Lexical approaches
    - compare frequencies of modifier and head candidates
- Context-based approaches
- Morphological approaches
- Phonological approaches


## Literature

- [Empirical Methods for Compound Splitting](http://homepages.inf.ed.ac.uk/pkoehn/publications/compound2003.pdf)
- [A corpus-based decompounding algorithm for German lexical modeling in LVCSR](https://perso.limsi.fr/madda/publications/PDF/ES031038.pdf)
- [Decompounding query keywords from compounding languages](http://www.aclweb.org/anthology/P08-2064)
- [Language-independent Compound Splitting with Morphological Operations](http://static.googleusercontent.com/media/research.google.com/de//pubs/archive/37093.pdf)

Assignment
================================================================================

Exercise 09 - Rule-based German decompounder
--------------------------------------------------------------------------------

As specified in these slides.

--------------------------------------------------------------------------------

Have fun!
