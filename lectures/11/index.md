---
title: 'Week 11: Unsupervised Learning'
author: Sebastian Ebert
date: 23/06/2015
---

Today
================================================================================

---

- Organizational things
- Presentation: Documentation
- Text Classification
- Naive Bayes

Organizational Things
================================================================================

---

- register for the exam
- semester projects
    - topics will be provided via email
    - due: Friday 24/07/2015, 23:59 CET
    - poster presentation: Thursday 30/07/2015


Presentation
================================================================================

- advanced regexes
- Data structures & complexities


Unsupervised Learning
================================================================================

What is Unsupervised Learning
--------------------------------------------------------------------------------

. . .

- no label given
- find common structure in data

---

![](../../res/img/unsupervised.png)\ 


General Examples
--------------------------------------------------------------------------------

. . .

- you see a group of people: divide them into groups
- cluster city names
- cluster trees

---

. . .

- document clustering
- unsupervised tokenization
- topic modeling (LSA, PLSA, LDA)
- dimensionality reduction (PCA, SVD)
- unsupervised word clustering (e.g., Brown clusters)
- deep learning techniques (e.g., AutoEncoders, word embeddings)


Difference to Text Classification Pipeline?
--------------------------------------------------------------------------------

![](../../res/img/supervised_ml.png)\ 


Unsupervised Machine Learning Methods
--------------------------------------------------------------------------------

- Which do you know?
- Which have you worked with?


Nearest Neighbors
================================================================================

Nearest Neighbors
--------------------------------------------------------------------------------

- fixed set of elements (e.g., documents): $D = \{d_1, \ldots, d_n\}$
- document $d$ is represented by a vector of features: \
  $d \in \mathbb{N}^k$ $\rightarrow$ $d = [x_1 x_2 \ldots x_k]$
- find the most similar document for a given document $d$

---

![](../../res/img/kmeans-initial.pdf)\ 


Requirements
--------------------------------------------------------------------------------

. . .

- metric for distance computation


Metrics
--------------------------------------------------------------------------------

- Euclidean: $\sqrt{\sum_{i=1}^k \left(x_i - y_i \right)^2}$
- Manhattan: $\sum_{i=1}^k \left|x_i - y_i \right|$
- Minkowski: $\left(\sum_{i=1}^k \left( \left| x_i - y_i \right| \right)^q \right)^{1/q}$

![](../../res/img/2D_unit_balls.pdf)\ [^1]

. . .

- cosine: $1 - \frac{X \cdot Y}{\parallel X \parallel \parallel Y \parallel}$

[^1]: https://en.wikipedia.org/wiki/Minkowski_distance 


$k$-Nearest Neighbors
--------------------------------------------------------------------------------

. . .

- take $k$ closest documents instead of only the closest
- more robust against outliers


k-means
================================================================================


k-means
--------------------------------------------------------------------------------

- clustering algorithm
- find cluster centroids
- chicken-egg problem

---

![](../../res/img/kmeans-initial.pdf)\ 

---

![](../../res/img/kmeans-2_clusters-0.pdf)\ 

---

![](../../res/img/kmeans-2_clusters-1.pdf)\ 

---

![](../../res/img/kmeans-2_clusters-2.pdf)\ 

---

1. randomly initialize cluster centroids
1. assign each document to a cluster
1. recompute cluster centroids
1. go back to 2 until nothing changes (or it takes too long)


Problems
--------------------------------------------------------------------------------

. . .

![](../../res/img/kmeans-3_clusters-2.pdf)\ 

---

![](../../res/img/kmeans-4_clusters-2.pdf)\ 

- How many clusters to use?

---

![](../../res/img/kmeans-empty_cluster.pdf)\ 

- How to initialize cluster centroids? (dead clusters)


Assignment
================================================================================

Exercise 10 - Polarity Classification
--------------------------------------------------------------------------------

1. Download the "word embeddings" file from the course website ([here](../../res/data/word_embs.tsv.bz2)).
  It contains words with a feature vector. Note that for testing, you can cut the file to a smaller size.
1. Implement 2 versions of $k$-nearest neighbors:
    1. The first version should print the $k$ nearest words for a given word.
    2. The second version should print the $k$ nearest words for a given feature vector of the same length as the other words.
1. Do not use an existing implementation of $k$-nearest neighbors, like sklearn.

more on the next slide!

---

4. Both applications must be parameterized, e.g., $k$ must be specified via parameter.
1. Create a log file that shows 2 input words each for which the 5 nearest neighbors make sense and do not make sense (4 in total).
1. Create a log file that shows the 5 nearest neighbors for the vector of "not" + "good" (element-wise sum).
1. Tag your commit in the repository.

Due: Thursday July 9, 2015, 16:00, i.e., the tag must point to a commit earlier than the deadline

--------------------------------------------------------------------------------

Have fun!
