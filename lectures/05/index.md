---
title: 'Week 05: Tidy up'
author: David Kaumanns
date: Mai 12, 2015
---


Today
================================================================================

---

- Presentations
- Summary & outlook
- Tidy up!
    - Structure & command line parameters


Presentations
================================================================================

---

The most dirty sentence segmentizer hack:

```
# Split while preserving split marker
my @sentences = split /(?=[!\?\.;]+\s*(?:[‹«"“”‘]?+\s*)?)/, $sentence;
```

What have we learned so far?
================================================================================

---

- Project management
    - Open source structure
    - Git & Gitlab
- Resources
    - Different types of corpora
    - External tools: CIS TreeTagger, Stanford Parser, Stanford CoreNLP

---

- Data pipeline for preprocessing
    - How to parse XML/Media Wiki
    - How to create clean training text
        - (it is hard!)
    - How to create vocabs/lexicons/dictionaries/wordlists/frequency lists
    - How to create annotated corpora
- Script magic
    - Serialization: binary & non-binary
    - Logger
    - Advances regexes (first part)

---

- Shell magic
    - Unicode & UTF-8
    - Shell tools: tr, sed, grep, awk, sort, uniq, find, iconv, cut, ...
    - Wrapping scripts with Make
        - (Wrapping Make with shell scripts)
    - [http://lifehacker.com/5743814/become-a-command-line-ninja-with-these-time-saving-shortcuts](Become a Command Line Ninja With These Time-Saving Shortcuts)

---

![](../../res/img/wrapper-structure.png)

---

This is not a programming course, \
it's a project course.

Outlook
================================================================================

---

- Crawling/scraping the web & APIs (Facebook, Twitter, ...)
- Linguistic algorithms
    - Running example: German decompounder
- Experiments & evaluation
- Performance optimization
- Statistical learning

. . .

... supported by your awesome presentations!


Tidy up
================================================================================

--------------------------------------------------------------------------------

- Structure
- Command line parameters in Make

--------------------------------------------------------------------------------

Have fun!
