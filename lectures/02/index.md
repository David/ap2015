---
title: 'Week 02: Resource pipeline'
author: David Kaumanns
date: April 21, 2015
---


Today
================================================================================

---

- NLP resources & formats
- DIY preprocessing
    - Lexicon
    - Corpus
    - Annotated corpus
- How to design a pipeline

Resources
================================================================================

---

![Which resources do you know?](../../../res/img/nlp-pipeline.png)


Formats
---

- XML
    - XHTML
    - JSON
    - YAML
- CSV
- Mediawiki
    - Google-flavored Mediawiki
- Markdown
    - Multimarkdown
    - Github-flavored Markdown
    - Pandoc

Lexicons
--------------------------------------------------------------------------------

- Multi-language frequency lists
    - <https://invokeit.wordpress.com/frequency-word-lists>
    - created from subtitles
- English frequency list
    - <http://www.wordfrequency.info/free.asp>
    - created from Corpus of Contemporary American English
- The CMU Pronouncing Dictionary
    - <http://www.speech.cs.cmu.edu/cgi-bin/cmudict>
- MPQA Subjectivity Clues
    - <http://mpqa.cs.pitt.edu/lexicons/subj_lexicon/>
- CISLex


Treebanks
--------------------------------------------------------------------------------

    (S
        (NP
            (NNP John)
        )
        (VP
            (VPZ loves)
            (NP
                (NNP Mary)
            )
        )
        (...)
    )

---

- The Penn Treebank Project
    - <https://www.cis.upenn.edu/~treebank/>
- German Treebank: Tiger
    - <http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/tiger.en.html>
- SMULTRON - Stockholm MULtilingual TReebank
    - <http://www.cl.uzh.ch/research/parallelcorpora/paralleltreebanks/smultron_en.html>
- University of Arizona TreeBank Viewer
    - <http://www.dingo.sbs.arizona.edu/~sandiway/treebankviewer/index.html>

Knowledge bases & ontologies
--------------------------------------------------------------------------------

- WordNet
    - <https://wordnet.princeton.edu>
- Germanet
    - <http://www.sfs.uni-tuebingen.de/GermaNet>
- Chinese Wordnet, BalkaNet, IndoWordNet, FinnWordNet, ...
- DBpedia
    - machine readable Wikipedia content
    - knowledge base of > 4 million "things"
    - <http://wiki.dbpedia.org/Ontology>

Parallel text corpora
--------------------------------------------------------------------------------

- Europarl
    - proceedings of the European Parliament in 21 languages
    - <http://www.statmt.org/europarl/>
- WMT11
    - from a shared task
    - <http://www.statmt.org/wmt11/translation-task.html#download>

Questions & answers
--------------------------------------------------------------------------------

- Microsoft Research Question-Answering Corpus
    - text of Encarta 98
    - <http://research.microsoft.com/en-us/downloads/88c0021c-328a-4148-a158-a42d7331c6cf/>
- PASCAL RTE datasets for textual entailment tasks
    - <http://pascallin.ecs.soton.ac.uk/Challenges/RTE/Datasets/>

Collocations & NGrams
--------------------------------------------------------------------------------

- Google Books Ngram Corpus
    - <http://storage.googleapis.com/books/ngrams/books/datasetsv2.html>
- <http://corpus.byu.edu>
- English collocations
    - <http://collocations.ooz.ie/>
- CIS Wittfind
    - <http://wittfind.cis.uni-muenchen.de>

Pretrained models & representations
--------------------------------------------------------------------------------

- Stanford Core NLP
    - tokenizer, sentence splitter, POS tagger, coreference resolution, etc.
    - <http://nlp.stanford.edu/software/corenlp.shtml>
- Polyglot word embeddings
    - low dimensional word representations for many Wikipedia languages
    - <https://sites.google.com/site/rmyeid/projects/polyglot#TOC-Download-the-Embeddings>

Text corpora
--------------------------------------------------------------------------------

- UMBC WebBase corpus
    - <http://ebiquity.umbc.edu/resource/html/id/351>
- News articles
    - Reuters news: <http://trec.nist.gov/data/reuters/reuters.html>
    - Wall Street Journal: <https://catalog.ldc.upenn.edu/LDC94S13A>
    - North American News Text Corpus: <https://catalog.ldc.upenn.edu/LDC95T21>
- Wikipedia

Wikipedia
--------------------------------------------------------------------------------

- Raw dumps: <https://dumps.wikimedia.org>
- Preprocessed Wikipedia dumps
    - <https://sites.google.com/site/rmyeid/projects/polyglot#TOC-Download-Wikipedia-Text-Dumps>
- Preprocessing scripts: Wikipedia Extractor
    - <http://medialab.di.unipi.it/wiki/Wikipedia_Extractor>

Let's design a pipeline
===

Core principles
---

- Make it modular
    - Not a giant god script
- Check for dependencies
- Keep it DRY (Don't Repeat Yourself)
- Shell or script doesn't matter
    - In fact, you can hardly avoid scripts at more complicated preprocessing, so rather go for a script right away if you know the complexity will increase later.
- Check for pre-existing tools (if the legal department allows it)

Visualize it
---

![Preprocessing pipeline](../../res/img/preprocessing-pipeline.png) \


Go for it
---

Wikipedia dumps: <http://dumps.wikimedia.org/enwiki/20150304>

<!--
Resources
================================================================================

Lexicons
--------------------------------------------------------------------------------

- [Multi-language frequency lists](https://invokeit.wordpress.com/frequency-word-lists)
- [English frequency list](http://www.wordfrequency.info/free.asp)
- [The CMU Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict)
- CISLex

Treebanks
--------------------------------------------------------------------------------

    (S
        (NP
            (NNP John)
        )
        (VP
            (VPZ loves)
            (NP
                (NNP Mary)
            )
        )
        (...)
    )

- [Deutsche Treebank: Tiger](http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/tiger.en.html)
- [SMULTRON - Stockholm MULtilingual TReebank](http://www.cl.uzh.ch/research/parallelcorpora/paralleltreebanks/smultron_en.html)
- [The Penn Treebank Project](https://www.cis.upenn.edu/~treebank/)
- [University of Arizona TreeBank Viewer](http://www.dingo.sbs.arizona.edu/~sandiway/treebankviewer/index.html)

Knowledge bases & ontologies
--------------------------------------------------------------------------------

- [Wordnet](https://wordnet.princeton.edu)
- [Germanet](http://www.sfs.uni-tuebingen.de/GermaNet)
- Chinese Wordnet, BalkaNet, IndoWordNet, FinnWordNet, ...
- [The DBpedia Ontology (2014)](http://wiki.dbpedia.org/Ontology)

Parallel text corpora
--------------------------------------------------------------------------------

- [Europarl](http://statmt.org)
- [WMT11](http://www.statmt.org/wmt11/translation-task.html#download) for various languages
    - ... of which [1 billion words preprocessed](http://www.statmt.org/lm-benchmark/)

Questions & answers
--------------------------------------------------------------------------------

- [Microsoft Research Question-Answering Corpus](http://research.microsoft.com/en-us/downloads/88c0021c-328a-4148-a158-a42d7331c6cf/)
- [PASCAL RTE datasets for textual entailment tasks]

Collocations & NGrams
--------------------------------------------------------------------------------

- [Google Ngrams](https://books.google.com/ngrams)
- <http://corpus.byu.edu>
- [CIS Wittfind](http://wittfind.cis.uni-muenchen.de)

Pretrained models & representations
--------------------------------------------------------------------------------

- [Polyglot word embeddings](https://sites.google.com/site/rmyeid/projects/polyglot#TOC-Download-the-Embeddings)

Text corpora
--------------------------------------------------------------------------------

- Websites
    - [UMBC WebBase corpus](http://ebiquity.umbc.edu/resource/html/id/351)
- News articles
    - [Reuters news](http://trec.nist.gov/data/reuters/reuters.html)
    - [Wall Street Journal](https://catalog.ldc.upenn.edu/LDC94S13A)
    - [North American News Text Corpus](https://catalog.ldc.upenn.edu/LDC95T21)
- Wikipedia

Wikipedia
--------------------------------------------------------------------------------

- Raw dumps: <https://dumps.wikimedia.org>
- [1 billion characters from 2006 Wikipedia](http://mattmahoney.net/dc/textdata.html)
    - Includes a Perl script to filter Wikipedia XML dumps
- [Preprocessed Wikipedia dumps](https://sites.google.com/site/rmyeid/projects/polyglot#TOC-Download-Wikipedia-Text-Dumps)
- [DBpedia](http://dbpedia.org)
    - Wikipedia, preprocessed and structured
    - Knowledge base of 4.58 million "things"
-->

Assignment
================================================================================

Exercise 02 - Resource pipeline
--------------------------------------------------------------------------------

Setup/finish you preprocessing pipeline. Design it in a modular way such that tasks that are independent are handled by separate calls (e.g. wget and subsequent preprocessing). Don't cram them all into one script. It should be very ease to choose another source and rerun your preprocessing pipeline.

So far, no third-party applications/scripts are allowed.

It is most important to setup a working pipeline than to create a perfect clean-up procedure. You may destroy sentences to a certain extent, as long as the final result is free of noise and garbage.

---

Pipeline:

1. Download these two sources:
    - <http://dumps.wikimedia.org/enwiki/20150304/enwiki-20150304-pages-articles1.xml-p000000010p000010000.bz2>
    - <http://dumps.wikimedia.org/enwiki/20150304/enwiki-20150304-pages-articles2.xml-p000010002p000024999.bz2>
1. Parse, extract and clean the text chunks (articles).

---

3. Create three resources:
    - Lexicon (as frequency list)
    - Clean corpus of ordered words. Feel free to organize it with additional markup to separate conceptual regions (e.g. articles in Wikipedia).
    - POS-tagged corpus (see below)

---

4. Download and use the CIS TreeTagger: <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger>
    - Note that you might have to reorganize your corpus to fit the input format of the TreeTagger.
1. Add, commit, push.

. . .

Due: Thursday April 30, 2015, 16:00

--------------------------------------------------------------------------------

Have fun
