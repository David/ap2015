---
title: 'Week 12: Language Modeling I'
author: Sebastian Ebert
date: 30/06/2015
---

Today
================================================================================

---

- Language Modeling
- Presentation: Smoothing


Language Modeling
================================================================================


What is a Language Model
--------------------------------------------------------------------------------

- given sentence $S = w_1w_2w_3 \ldots w_n = w_1^{n}$

. . .

- $P(S) = P(w_1, w_2, w_3, \ldots, w_n) = P(w_1^{n})$ or
- $P(w_n | w_1, w_2, w_3, \ldots, w_{n-1}) = P(w_n | w_1^{n - 1})$

- each of both is called language model


Applications
--------------------------------------------------------------------------------

. . .

- measure the correctness of the produced text
- machine translation: scoring hypotheses
- speech recognition: scoring hypotheses
- generative summarization: create a summary that has correct grammar
- spelling correction: which spelling in a context is more likely


How it Works
--------------------------------------------------------------------------------

- chain rule of probability

\begin{align*}
    P(w_1^{4}) & = P(w_1) P(w_2 | w_1) P(w_3 | w_1^{2}) P(w_4 | w_1^{3}) \\
    P(w_1^{n}) & = \prod_i P(w_i | w_1^{i-1})
\end{align*}

. . .

- estimate possibilities by counting (maximum likelihood)

\begin{align*}
    P(w_5 | w_1^{4}) & = \frac{count(w_1^{5})}{count(w_1^{4})} \\
    P(w_n | w_1^{n-1}) & = \frac{count(w_1^{n})}{count(w_1^{n - 1})}
\end{align*}


Problem
--------------------------------------------------------------------------------

\begin{align*}
    P(w_n | w_1^{n-1}) & = \frac{count(w_1^{n})}{count(w_1^{n - 1})}
\end{align*}

- assume $P(w_{20} | w_1^{19})$: problem?

. . .

- too many possible sequences
- not enough data to estimate the probabilities


Solution: Markov Assumption
--------------------------------------------------------------------------------

\begin{align*}
    P(w_n | w_1^{n-1}) \approx P(w_n | w_{n-k+1}^{n-1})
\end{align*}

- = **ngram** model

. . .

- "I want chinese food"

. . .

- unigram ($k=1$): $P(w_5 | w_1^{4}) \approx P(w_5)$
    $P(food | \text{i want chinese}) \approx P(food)$

. . .

- bigram ($k=2$): $P(w_5 | w_1^{4}) \approx P(w_5 | w_4)$
    $P(food | \text{i want chinese}) \approx P(food | \text{chinese})$

. . .

- trigram($k=3$): $P(w_5 | w_1^{4}) \approx P(w_5 | w_3^4)$
    $P(food | \text{i want chinese}) \approx P(food | \text{want chinese})$


Example
--------------------------------------------------------------------------------

- Wall Street Journal corpus
- 42068 sentences
- vocabulary: $V = \{i, want, to, read, a, book\}$
- unigram counts

 i    want   to      read   a       book
---- ------ ------- ------ ------- ------
1262  292    23636   78    21195    81

---

- bigram counts

c(row|col)    i     want    to    read   a    book
----------- ------ ------  ----- ------ ---- ------
i             1     0       0      1     1    0
want         12     0       2      0     0    0
to            1     184     2      1     1    1
read          0     0       19     0     1    0
a             0     10      578    3     1    1
book          0     0       1      0     12   0

---

- normalized by unigram counts, i.e., $P(row|col)$

P(row|col)   i          want       to         read       a          book
----------- --------   --------   --------   --------   --------   -------
i            0.0008     0          0          0.0128     0          0     
want         0.0095     0          0.0001     0          0          0     
to           0.0008     0.6301     0.0001     0.0128     0          0.0123
read         0          0          0.0008     0          0          0     
a            0          0.0342     0.0245     0.0385     0          0.0123
book         0          0          0          0          0.0006     0     

---

- probability of the phrase "i want a book" when using a bigram model:

\begin{align*}
    P(\text{i want a book}) = P(want | i) P(a | want) P(book | a)
\end{align*}

. . .

- problem: How to estimate the probability that "I" is the first word or that "book" is the last word?

. . .

- solution: add special sentence beginning and sentence end tags
    "i want a book" $\rightarrow$ "\<s\> i want a book \</s\>"

---

\footnotesize

 i    want   to      read   a       book   \<s\>   \</s\>
---- ------ ------- ------ ------- ------ ------- -------
1262  292    23636   78    21195    81     42068   42068


c(row|col)   i          want       to         read       a          book    \<s\>
----------- --------   --------   --------   --------   --------   ------- -------
i            0.0008     0          0          0.0128     0          0       0.0127
want         0.0095     0          0.0001     0          0          0       0
to           0.0008     0.6301     0.0001     0.0128     0          0.0123  0.0036
read         0          0          0.0008     0          0          0       0
a            0          0.0342     0.0245     0.0385     0          0.0123  0.0204
book         0          0          0          0          0.0006     0       0
\</s\>       0          0.0137     0.0011     0.0897     0          0.0617  0

---

$S = \text{"<s> i want a book </s>"}$

\begin{align*}
    P(S) = & P(i | \text{<s>}) P(want | i) P(a | want) P(book | a) P(\text{</s>} | book) \\
     = & 1.53e-10
\end{align*}


Problems
--------------------------------------------------------------------------------

. . .

- sparsity
    - the higher $n$ the less "dense" our counts are
    - i.e., you do not have enough training data to have reliable estimates
    - many of your count table entries are zero
    - i.e., you want a small $n$

. . .

- long distance dependencies
     - "The computer which I had just put into the machine room on the fifth floor crashed."
     - worse in German (e.g., separable verbs, nested sentences)

---

- "underflow"
    - multiplication of many small probabilities leads to zero probabilities
    - solution: work in log space
    - $p_1 \times p_2 \times p_3 = \log p_1 + \log p_2 + \log p_3$
    - $P(S) = \prod_i P(w_i | w_1^{i-1})$
    - $\log P(S) = \sum_i \log P(w_i | w_1^{i-1})$


Evaluation
================================================================================


What is a Good Language Model?
--------------------------------------------------------------------------------

. . .

- high probability for "real" or "frequently observed" sentences
- low probability for "ungrammatical" or "rarely observed" sentences
- high probability for the "correct" word after a given sequence


Extrinsic Evaluation
--------------------------------------------------------------------------------

- best: extrinsic evaluation, i.e., use language model in specific task
    - machine translation
    - speech recognition
    - spelling corrector
- calculate accuracy of language models A and B on this task
- compare
- take the better one

. . .

- problems
    - time consuming: train an MT or ASR system


Intrinsic Evaluation
--------------------------------------------------------------------------------

- intrinsic evaluation, i.e., compute performance of language model itself
- i.e., compute the probability that the language model gives the test set


The Shannon Game
--------------------------------------------------------------------------------

- How well can we predict the next word?

    - "I study CL at the university of ???"
    - "I always order pizza with cheese and ???"

- good language model: assign a higher probability to the "correct" word
- Why is unigram model bad at that?


Perplexity
--------------------------------------------------------------------------------

- perplexity is the probability of the test set, normalized by the number of words

\begin{align*}
    S & = w_1 w_2 \ldots w_n \\
    PP(S) & = P(S)^{-\frac{1}{n}} = \sqrt[n]{\frac{1}{P(S)}} = \sqrt[n]{\frac{1}{\prod_i P(w_i | w_1^{i-1})}} \\
\end{align*}

\begin{align*}
    PP_{bigram}(S) & = \sqrt[n]{\frac{1}{\prod_i P(w_i | w_{i-1})}} \\
\end{align*}


Intuition
--------------------------------------------------------------------------------

- assume a sentence consisting of random digits
- What is the perplexity of this sentence according to a model that assigns $P=1/10$ to each digit?

    $PP(S) = P(w_1^n)^{-\frac{1}{n}} = \left( \frac{1}{10}^n \right)^{- \frac{1}{n}} = \frac{1}{10}^{-1} = 10$

- informally it means: how many words will the language model think are possible
- i.e., lower is better

---

- example Wall Street Journal
    - PP unigram model: 962
    - PP bigram model: 170
    - PP trigram model: 109

--------------------------------------------------------------------------------

Have fun!
