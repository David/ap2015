# Syllabus

## Semester projects

Bachelor: Web crawler + preprocessing
Master: Language model

## Done

- Pipelines & applications

- Open source projects
- Git & Gitlab

- NLP Resources
- Formats: Raw, Mediawiki, ...
- Pipeline part 1: creating some dirty resources from raw corpora

## To do

(P) marks presentation candidates

---

- Unicode (P)
- Makefiles (P)

- Readme: Markdown

- Stanford Core NLP
- How to preserve sentences
- Lessons learned: preprocessing is hard

- Normalization
- Segmentation
- Tokenization
- (Semi-)Automatic annotation

---

- Shell magic: tr, sed, grep, awk, sort, uniq, find, iconv, man pages (P)
- Binary serialization: Python pickle, Perl (?), Matlab, ... (P)
- Non-binary serialization: XML, JSON, YAML, CSV (P)

---

- Documentation: Python (Sphinx) (P)
    - Javadoc, Doxygen, Perldoc, [Docutils](http://docutils.sourceforge.net)
- Logging: Python (Logger), ... (P)
- (Perl) regular expressions for pros
    - Lookahead/behind (P)
        - Reverse string for negative look-behind
    - Greedy vs. non-greedy
    - Sentence splitter
    - Flop-flop operator
    - Regex for XML: why is that a bad idea?
    - (.+?)\..+$ vs. ([^\.]+)\..+$
        - foo.bar.qux

- The proper language for the right task

### Crawler

- HTML parsing (P)
    - DOM, SAX, XX
- Crawler basics (P)
    - Netiquette
        - Twitter crawling
        - Robots.txt
        - Google threshold
    - Web standards: problems of crawling, improper HTML, ... "World Wild Web"
    - Libraries: Perl (Web scraper), Python (Beautifulsoup), ...

- Data annotation

- Running example: news


- Dictionaries and lexica
- Filtering corpora against wordlists
- The problem of topical bias

- Complex data structures: set, list, hashmap, tables, tensors, ...
- Advanced sorting (e.g. Schwartzian transform)
- Object oriented programming: Multiple inheritance, polymorphism, overloading, encapsulation, traits/mixins/roles
- Functional programming: function references and callbacks, lambda expressions, closures
- Declarative programming: lazyness, type-checking & verification
- Performance optimization: caching, multithreading
- Visualization: dot graphs

- Stemming
- Lemmatization
- Decompounding
- Local grammars & information extraction

- Classification & Clustering: Naive Bayes, SVM, K-nearest
- Decision trees

- Unit tests
- Training set, validation set, test set
- Precision, Recall, F-measure
- Cross-validation
- Perplexity

- Hidden Markov Models & n-grams
- Artificial neural networks


# Grading

## Hard Criteria

- Does the program compile?
- Does the project have a sensible/conventional structure?
- Does the input/output interface adhere the given specifications?
- Are dependencies automatically resolved or exhaustively listed?
- Does the test perplexity reach the specified threshold?
- Does it exclusively use helper libraries that were allowed (e.g. math libraries)?

## Soft Criteria

- Project structure: Are object orientation and functional design patterns used in a sensible way?
- No code smell: Are variables consistent and sensible? Do they adhere to common best practices?
- Performance: Does it scale well (enough)?
- Sustainability: Is the code readable and maintainable?

# Recommendations

Tools:

Python/ Perl/ Ruby
Lua/ Torch
Python Theano
Makefile
C/ C++
Java

Questions:

GUI
GPU
Speed
Parallelization
Development overhead
Startup time vs running time

# Semester project

## Bachelor: Resource pipeline

Setup a resource pipeline from a given set of links to the final sets for training, testing and validation.
The pipeline must be organized via make-connected scripts and a configuration file. It includes:

- Download
- Parsing
- Cleaning: linguistic and non-linguistic preprocessing
    - Sentences must be kept intact! Do not just remove text chunks that are hard to parse.
- Concatenation
- Split into training set, validation set and test set.
- Vocabulary with frequencies

Grading is based on:

- the specified hard and soft criteria
- The cleanness of the resulting data sets in comparison to our golden standard

Students are welcome to upgrade their semester task to the Master's topic.

---

## Master: Dream machine

Implement and train a language model on a given set of data. The model's purpose is to suggest next words based on a given context (as it is used e.g. in mobile text processing systems). On order to assess the sensibility of the suggestions, we need a "dream mode" for generating word sequences.

- Implementation of one specific language model algorithm
- Training, validation and testing with perplexities
- Dream mode: a well-trained model is able to create a stream of words that is syntactically quite correct, though maybe semantically questionable.

Grading is based on:

- the specified hard and soft criteria
- the minimum test perplexity.

