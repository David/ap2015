
import logging
from logging import Formatter

my_logger = logging.getLogger('MyLogger')

formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s in %(pathname)s')
file_handler = logging.FileHandler('formatted_log')
file_handler.setLevel(logging.INFO)

file_handler.setFormatter(formatter)

my_logger.addHandler(file_handler)

my_logger.error('message')