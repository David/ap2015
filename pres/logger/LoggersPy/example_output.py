
import logging
import sys

LOG_FILENAME = 'example_log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(message)s in %(pathname)s')


logging.debug ('Debug Message')
logging.info ('Info Message')
logging.warning('Warning Message')
logging.error('Error Message')

try:        
	100 / 0
except ZeroDivisionError:
	logging.critical('Failed', exc_info=True)