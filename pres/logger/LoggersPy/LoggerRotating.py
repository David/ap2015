
import logging
import logging.handlers

LOGGING_OUT = 'rotating_file.log'

my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)

rf_handler = logging.handlers.RotatingFileHandler(LOGGING_OUT, maxBytes=20, backupCount=3)
my_logger.addHandler(rf_handler)

my_logger.debug('test')