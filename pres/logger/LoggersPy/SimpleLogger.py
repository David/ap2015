
import logging

LOG_FILENAME = 'simple_logger_out.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.INFO,format='%(asctime)s %(levelname)s: %(message)s in %(pathname)s')

logging.info ('Info to Logfile')
